/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyBusiness.Role.WorkRequest;

import Business.Employee.Employee;
import Business.WorkQueue.WorkRequest;

/**
 *
 * @author Vasanti
 */
public class DonorWorkRequest extends WorkRequest{
    private String barcode;
    private String bloodType;
    private Employee employee;
    private String expirationDate;
    private String sendTo;
    public String getBarcode() {
        return barcode;
    }

    @Override
    public String toString() {
        return employee.getName() ;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getSendTo() {
        return sendTo;
    }

    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }
    
    
    
}
