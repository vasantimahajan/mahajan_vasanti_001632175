/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Salesperson;

import Business.CatalogueDirectory;
import Business.Customer;
import Business.LineItem;
import Business.Order;
import Business.OrderCatalogue;
import Business.Product;
import Business.Salesperson;
import Business.Supplier;
import Business.SupplierDirectory;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Vasanti
 */
public class PlaceOrder extends javax.swing.JPanel {

    /**
     * Creates new form PlaceOrder
     */
    private JPanel userProcessContainer;
    private Salesperson salesperson;
    private Customer customer;
    private CatalogueDirectory catDir;
    private SupplierDirectory supDir;
    private OrderCatalogue masterOrderCatalogue;
    boolean isCheckedOut = false;
    private Order order;
    public PlaceOrder(JPanel userProcessContainer, Salesperson salesperson, Customer customer, CatalogueDirectory catDir,SupplierDirectory supDir,OrderCatalogue masterOrderCatalogue) {
         initComponents();
         this.userProcessContainer=userProcessContainer;
         this.salesperson=salesperson;
         this.customer=customer;
         this.supDir=supDir;
         this.catDir=catDir;
         this.masterOrderCatalogue=masterOrderCatalogue;
        
          populateSupplierCombo();
          
    }
    
    public void populateSupplierCombo()
    {
       
        suppComboBox1.removeAllItems();

        for (Supplier supplier : supDir.getSupplierList()) {

            
            suppComboBox1.addItem(supplier);
        }
        populateProductTable();
        if (!isCheckedOut) {
            order = new Order();
        }
    }
    
    public void populateProductTable()
    {
        DefaultTableModel dtm = (DefaultTableModel) productTable.getModel();
        Supplier supplier = (Supplier) suppComboBox1.getSelectedItem();
        dtm.setRowCount(0);
        if (supplier != null) {
            for (Product product : supplier.getProductCatalog().getProductList())
            {
                Object row[] = new Object[6];
                row[0] = product;
                row[1] = product.getModelNumber();
                row[2] = product.getFloorPrice();
                row[3] = product.getCeilingPrice();
                row[4] = product.getTargetPrice();
                row[5] = product.getAvail();
                dtm.addRow(row);
            }
        }
    }
    
    public void refreshProductTable(String keyWord)
    {
    int rowCount = productTable.getRowCount();
        DefaultTableModel model = (DefaultTableModel) productTable.getModel();
        for (int i = rowCount - 1; i >= 0; i--) {
            model.removeRow(i);
        }

        for (Supplier s : supDir.getSupplierList())
        {
            for (Product p : s.getProductCatalog().getProductList())
            {
                if (keyWord.equals(p.getProdName()))
                {
                    Object row[] = new Object[6];
                    row[0] = p;
                    row[1] = p.getModelNumber();
                    row[2] = p.getFloorPrice();
                    row[3] = p.getCeilingPrice();
                    row[4] = p.getTargetPrice();
                    row[5] = p.getAvail();

                    model.addRow(row);
                }
            }
        }
    }
            
    public void refreshOrderTable()
    {
        int rowCount = orderTable.getRowCount();
        for (int i = rowCount - 1; i >= 0; i--) {
            ((DefaultTableModel) orderTable.getModel()).removeRow(i);
        }

        for (LineItem li : order.getLineItemList()) {
            Object row[] = new Object[4];
            row[0] = li;
            row[1] = li.getActualPrice();
            row[2] = li.getQuantity();
            row[3] = li.getActualPrice() * li.getQuantity();
            ((DefaultTableModel) orderTable.getModel()).addRow(row);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        productTable = new javax.swing.JTable();
        suppComboBox1 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        addtoCartButton6 = new javax.swing.JButton();
        quantitySpinner = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        btnSearchProduct = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtActualPrice = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        orderTable = new javax.swing.JTable();
        btnModifyQuantity = new javax.swing.JButton();
        btnRemoveOrderItem = new javax.swing.JButton();
        btnCheckOut = new javax.swing.JButton();
        txtSearchKeyWord = new javax.swing.JTextField();
        txtNewQuantity = new javax.swing.JTextField();
        actualPriceError = new javax.swing.JLabel();
        quantityError = new javax.swing.JLabel();
        modifyQuantityError = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        commissionTF = new javax.swing.JTextField();

        productTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        productTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Model number", "Floor Price", "Ceiling price", "Target price", "Availability"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(productTable);

        suppComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                suppComboBox1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Supplier");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Supplier Product Catalog");

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        addtoCartButton6.setText("ADD TO CART");
        addtoCartButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addtoCartButton6ActionPerformed(evt);
            }
        });

        quantitySpinner.setModel(new javax.swing.SpinnerNumberModel());

        jLabel5.setText("Quantity:");

        btnSearchProduct.setText("Search Product By Name");
        btnSearchProduct.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchProductActionPerformed(evt);
            }
        });

        jLabel6.setText("Actual Price");

        jLabel7.setText("Item in cart");

        orderTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Item Name", "Price", "Quantity", "Total Amount"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(orderTable);

        btnModifyQuantity.setText("Modify Quantity");
        btnModifyQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModifyQuantityActionPerformed(evt);
            }
        });

        btnRemoveOrderItem.setText("Remove");
        btnRemoveOrderItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveOrderItemActionPerformed(evt);
            }
        });

        btnCheckOut.setText("Check out");
        btnCheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckOutActionPerformed(evt);
            }
        });

        jLabel3.setText("Commission:");

        commissionTF.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(modifyQuantityError, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(146, 146, 146))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(suppComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(55, 55, 55)
                                    .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btnSearchProduct))
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(76, 76, 76)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(151, 151, 151)
                                            .addComponent(addtoCartButton6))
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel5)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(quantityError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGap(37, 37, 37)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel6)
                                                    .addGap(29, 29, 29)
                                                    .addComponent(txtActualPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(actualPriceError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGap(42, 42, 42)
                                            .addComponent(jLabel3)
                                            .addGap(18, 18, 18)
                                            .addComponent(commissionTF, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 549, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnModifyQuantity)
                                .addGap(18, 18, 18)
                                .addComponent(txtNewQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(49, 49, 49)
                                .addComponent(btnCheckOut)
                                .addGap(31, 31, 31)
                                .addComponent(btnRemoveOrderItem))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 549, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(suppComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearchKeyWord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearchProduct))
                .addGap(33, 33, 33)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(quantitySpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtActualPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(commissionTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(actualPriceError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(quantityError, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addtoCartButton6)
                .addGap(19, 19, 19)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModifyQuantity)
                    .addComponent(txtNewQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCheckOut)
                    .addComponent(btnRemoveOrderItem))
                .addGap(26, 26, 26)
                .addComponent(btnBack)
                .addGap(66, 66, 66)
                .addComponent(modifyQuantityError, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void suppComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_suppComboBox1ActionPerformed
        // TODO add your handling code here:
        populateProductTable();
    }//GEN-LAST:event_suppComboBox1ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        if (order.getLineItemList().size() > 0) 
        {
            for (LineItem lineItem : order.getLineItemList())
            {
                Product p = lineItem.getProduct();
                p.setAvail(lineItem.getQuantity() + p.getAvail());
            }
            order.getLineItemList().removeAll(order.getLineItemList());
        }
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void addtoCartButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addtoCartButton6ActionPerformed
        // TODO add your handling code here:

        // TODO add your handling code here:
        int selectedRow = productTable.getSelectedRow();
        Product selectedProduct;
        int actualPrice;
        double commission=0.0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        else {
            
            selectedProduct = (Product) productTable.getValueAt(selectedRow, 0);
           
        }

        try {
            actualPrice = Integer.parseInt(txtActualPrice.getText());
        } 
        catch (Exception e)
        {
           actualPriceError.setText("Enter a valid value"); 
            return;
        }

        int fetchedQty = (Integer) quantitySpinner.getValue();
        if (fetchedQty <= 0) 
        {
            quantityError.setText("Select atleast 1 quantity");
            return;
            
        }
        else if (fetchedQty <= selectedProduct.getAvail()) {
            boolean alreadyPresent = false;
            for (LineItem lineItem : order.getLineItemList()) {
                if (lineItem.getProduct() == selectedProduct) 
                {
                    int oldAvail = selectedProduct.getAvail();
                    int newAvail = oldAvail - fetchedQty;
                    selectedProduct.setAvail(newAvail);
                    lineItem.setQuantity(fetchedQty + lineItem.getQuantity());
                    alreadyPresent = true;
                    refreshOrderTable();
                    populateProductTable();
                    
                  
                    break;
                }
            }

            if (!alreadyPresent) {
                int oldAvail = selectedProduct.getAvail();
                int newAvail = oldAvail - fetchedQty;
                selectedProduct.setAvail(newAvail);
                LineItem li=order.addLineItem(selectedProduct, fetchedQty, actualPrice);
                caluculateCommission(li);
                li.setProduct(selectedProduct);
               // selectedProduct.getLineitemList().add(li);
                commissionTF.setText(Double.toString(li.getComission()));
                
                refreshOrderTable();
                populateProductTable();
            }
        }
        else
        {
            quantityError.setText("The quantity is greater than the availability");
           
        }
        
    }//GEN-LAST:event_addtoCartButton6ActionPerformed

    public Double caluculateCommission(LineItem lineitem)
    {
        double commission=0.0;
       
                if(lineitem.getActualPrice()>=lineitem.getProduct().getFloorPrice())
                {
                    if(lineitem.getActualPrice()<lineitem.getProduct().getTargetPrice())
                    {
                    
                    commission=commission+(0.05*lineitem.getActualPrice());
                    lineitem.setComission(commission);
                  
                    }
                    if(lineitem.getActualPrice()>=lineitem.getProduct().getTargetPrice() && lineitem.getActualPrice()<lineitem.getProduct().getCeilingPrice())
                    {
                    commission=commission+(0.10*lineitem.getActualPrice());
                    lineitem.setComission(commission);
                 
                    }
                    if(lineitem.getActualPrice()>=lineitem.getProduct().getCeilingPrice())
                    {
                    commission=commission+(0.25*lineitem.getActualPrice());
                    lineitem.setComission(commission);
               
                    }
                    
                }
                else
                {
                    commission=commission+(0.02*lineitem.getActualPrice());
                    lineitem.setComission(commission);
                   
                }
                
        return commission;
    }
    private void btnSearchProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchProductActionPerformed
        String keyWord = txtSearchKeyWord.getText();
        refreshProductTable(keyWord);
    }//GEN-LAST:event_btnSearchProductActionPerformed

    private void btnModifyQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModifyQuantityActionPerformed
        // TODO add your handling code here:
        int selectedRow = orderTable.getSelectedRow();
        //Product selectedProduct;
        //int salesPrice=0;
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (!txtNewQuantity.getText().isEmpty() && !txtNewQuantity.getText().equals("0")) {
            LineItem lineItem = (LineItem) orderTable.getValueAt(selectedRow, 0);
            int currentAvail = lineItem.getProduct().getAvail();
            int oldQty = lineItem.getQuantity();
            int newQty = Integer.parseInt(txtNewQuantity.getText());
            if (newQty > (currentAvail + oldQty))
            {
                modifyQuantityError.setText("Quantity is more than the availability");
      
            }
            else
            {
                if (newQty <= 0)
                {
                    modifyQuantityError.setText("Enter a valid quantity");
                    return;
                }
                lineItem.setQuantity(newQty);
                lineItem.getProduct().setAvail(currentAvail + (oldQty - newQty));
                refreshOrderTable();
                populateProductTable();
            }
        } 
        else
        {
            modifyQuantityError.setText("Quantity cannot be zero");
        }
    }//GEN-LAST:event_btnModifyQuantityActionPerformed

    private void btnRemoveOrderItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveOrderItemActionPerformed
        int selectedRowCount = orderTable.getSelectedRowCount();
        if (selectedRowCount <= 0) {
            JOptionPane.showMessageDialog(null, "You didn't select any rows from the lineItem table");
            return;
        }

        int row = orderTable.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Failed to retrive selected row");
            return;
        }

        LineItem li = (LineItem) orderTable.getValueAt(row, 0);
        int oldQuantity = li.getProduct().getAvail();
        int orderQuantity = li.getQuantity();
        int newQuantity = oldQuantity + orderQuantity;
        li.getProduct().setAvail(newQuantity);
        order.removeLineItem(li);
        JOptionPane.showMessageDialog(null, "The order item of " + orderQuantity + "of " + li + " has been removed.");
        refreshOrderTable();
        populateProductTable();
    }//GEN-LAST:event_btnRemoveOrderItemActionPerformed

    private void btnCheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckOutActionPerformed
        // TODO add your handling code here:
        if (order.getLineItemList().size() > 0) {

            System.err.println(order.getLineItemList());
            masterOrderCatalogue.getOrderList().add(order);
            salesperson.getOrderCatalogue().addOrder(order);
            customer.getOrderCatalogue().addOrder(order);
            order.setCustomer(customer);
            order.setSalesperson(salesperson);
            //salesperson.getOrderCatalogue().addOrder(order);
            System.out.println("the salesperson order list has order"+salesperson.getOrderCatalogue().getOrderList());
            isCheckedOut = true;
            for(LineItem li:order.getLineItemList())
            {
               if(li.getProduct().getLineitemList().isEmpty())
               {
                   double newtotal=li.getActualPrice()*li.getQuantity();
//                   double oldTotal=li.getProduct().getSalesVolume();
                   li.getProduct().setSalesVolume(newtotal);
                   li.getProduct().getLineitemList().add(li);
                  // li.getProduct().addLineitem(li);
               }
               else
               { 
                 for(LineItem line:li.getProduct().getLineitemList())
                 {
                     if(line.getProduct().getModelNumber()==(li.getProduct().getModelNumber()))
                     {
                         double newTotal=li.getActualPrice()*li.getQuantity();
                         double oldTotal=line.getProduct().getSalesVolume();
                         li.getProduct().setSalesVolume(oldTotal+newTotal);
                     }
                     
                     else
                     {
                         double newTotal=li.getActualPrice()*li.getQuantity();
                          li.getProduct().setSalesVolume(newTotal);
                          li.getProduct().getLineitemList().add(li);
                     }
                 }
               }
               
            }
         
            
            JOptionPane.showMessageDialog(null, "Order added successfully!!");
            btnBack.setEnabled(false);
            order = new Order();
            refreshOrderTable();
            populateProductTable();
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Order not added as there are no items!!");
        }
    }//GEN-LAST:event_btnCheckOutActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel actualPriceError;
    private javax.swing.JButton addtoCartButton6;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCheckOut;
    private javax.swing.JButton btnModifyQuantity;
    private javax.swing.JButton btnRemoveOrderItem;
    private javax.swing.JButton btnSearchProduct;
    private javax.swing.JTextField commissionTF;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel modifyQuantityError;
    private javax.swing.JTable orderTable;
    private javax.swing.JTable productTable;
    private javax.swing.JLabel quantityError;
    private javax.swing.JSpinner quantitySpinner;
    private javax.swing.JComboBox suppComboBox1;
    private javax.swing.JTextField txtActualPrice;
    private javax.swing.JTextField txtNewQuantity;
    private javax.swing.JTextField txtSearchKeyWord;
    // End of variables declaration//GEN-END:variables
}
