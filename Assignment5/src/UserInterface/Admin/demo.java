/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Admin;

import Business.Product;
import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public abstract class demo {
private String name; 
private ArrayList<Product> products;   
    
    
public demo(){
       
products = new ArrayList<Product>();
 
    }

    public abstract ArrayList getProductsBelowPrice(int n);
        
    public void setName(String n){
           
        name = n;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

}
