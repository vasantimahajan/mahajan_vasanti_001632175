/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Admin;

import Business.SalesPersonDirectory;
import Business.Salesperson;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Vasanti
 */
public class ManageSalesPerson extends javax.swing.JPanel {

    /**
     * Creates new form ManageSalesPerson
     */
    private SalesPersonDirectory salesPersonDirectory;
    private JPanel userProcessContainer;
    public ManageSalesPerson(SalesPersonDirectory salesPersonDirectory,JPanel userProcessContainer) {
        initComponents();
        this.salesPersonDirectory=salesPersonDirectory;
        this.userProcessContainer=userProcessContainer;
        populateTable();
        hideComponents();
        
    }
    
    public void hideComponents()
    {
        nameLable.setVisible(false);
        nameTF.setVisible(false);
        addButton.setVisible(false);
    }
    
    public void unhideComponents()
    {
        nameLable.setVisible(true);
        nameTF.setVisible(true);
        addButton.setVisible(true);
    }
    
    public void clearFields()
    {
        nameTF.setText("");
    }
    
    public void populateTable()
    {
         DefaultTableModel dtm=(DefaultTableModel) salesPersonTable.getModel();
            int rowCount = salesPersonTable.getRowCount();
            for(int i = rowCount - 1; i>=0; i--)
            {
                dtm.removeRow(i);
             }
      
        
            for(Salesperson s: salesPersonDirectory.getSalesPersonList())
            {
                Object row[] =new Object[2];
                row[0]=s.getName();
                row[1]=s;
                dtm.addRow(row);
 
           }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        addSalespersonButton = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        salesPersonTable = new javax.swing.JTable();
        nameLable = new javax.swing.JLabel();
        nameTF = new javax.swing.JTextField();
        addButton = new javax.swing.JButton();
        back = new javax.swing.JButton();

        jLabel1.setText("Manage Sales Person");

        addSalespersonButton.setText("Add salesperson");
        addSalespersonButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSalespersonButtonActionPerformed(evt);
            }
        });

        jButton2.setText("Delete salesperson");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        salesPersonTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Salesperson name", "Salesperson Id"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(salesPersonTable);

        nameLable.setText("Name:");

        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        back.setText("Back");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(149, 149, 149)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(addSalespersonButton)
                        .addGap(46, 46, 46)
                        .addComponent(jButton2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(nameLable)
                        .addGap(52, 52, 52)
                        .addComponent(nameTF, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(back)
                        .addGap(92, 92, 92)
                        .addComponent(addButton))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(62, 62, 62)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(66, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(addSalespersonButton))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nameLable)
                    .addComponent(nameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(addButton)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                        .addComponent(back)
                        .addGap(37, 37, 37))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addSalespersonButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSalespersonButtonActionPerformed
        // TODO add your handling code here:
        unhideComponents();
        addSalespersonButton.setEnabled(false);
    }//GEN-LAST:event_addSalespersonButtonActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        // TODO add your handling code here:
        String name=nameTF.getText();
        salesPersonDirectory.addSalesPerson(name);
        JOptionPane.showMessageDialog(null,"Salesperson successfully created");
        clearFields();
        hideComponents();
        addSalespersonButton.setEnabled(true);
        populateTable();
    }//GEN-LAST:event_addButtonActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        
        int selectedRow = salesPersonTable.getSelectedRow();
        if(selectedRow >= 0)
        {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Do you want to delete this salesperson?", "Warning", JOptionPane.WARNING_MESSAGE);
            if(dialogResult == JOptionPane.YES_OPTION) {
                Salesperson sales = (Salesperson)salesPersonTable.getValueAt(selectedRow, 0);
                salesPersonDirectory.deleteSalesPerson(sales);
                populateTable();
            } 
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Please select a row from the table to delete a salesperson", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JButton addSalespersonButton;
    private javax.swing.JButton back;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel nameLable;
    private javax.swing.JTextField nameTF;
    private javax.swing.JTable salesPersonTable;
    // End of variables declaration//GEN-END:variables
}
