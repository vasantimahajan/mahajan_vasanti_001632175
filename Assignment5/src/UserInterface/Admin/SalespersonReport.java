/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Admin;

import Business.LineItem;
import Business.Order;
import Business.Product;
import Business.SalesPersonDirectory;
import Business.Salesperson;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Vasanti
 */
public class SalespersonReport extends javax.swing.JPanel {

    /**
     * Creates new form SalespersonReport
     */
    private SalesPersonDirectory salesPersonDirectory;
    private JPanel userProcessContainer;
    private Salesperson sales;
    private int countForAbove;
    private int countForBelow;

    private double first;
    private double second;
    private double third;
    ArrayList<Salesperson> topList;

    public SalespersonReport(SalesPersonDirectory salesPersonDirectory,JPanel userProcessContainer) {
        initComponents();
        this.salesPersonDirectory=salesPersonDirectory;
        this.userProcessContainer=userProcessContainer;
       // sales=new Salesperson();
        populateSalesPersonCombo();
        //populateTable();
        caluculateTheTopThreeSalesPerson();
        topList=new ArrayList<Salesperson>();
        
    }
    
    public void caluculateTheTopThreeSalesPerson()
    {   
        for(Salesperson sp:salesPersonDirectory.getSalesPersonList())
            
        {  double totalSalesOfSalesPerson=0.0;            
           double totalCostOfAOrder=0.0;
            
            for(Order o:sp.getOrderCatalogue().getOrderList())
            {   double totalCostOfALineItem=0.0;
               
                for(LineItem li:o.getLineItemList())
                { 
                    totalCostOfALineItem=totalCostOfALineItem+(li.getActualPrice()*li.getQuantity());
                }
                totalCostOfAOrder=totalCostOfAOrder+totalCostOfALineItem;
                
            }
            totalSalesOfSalesPerson=totalSalesOfSalesPerson+totalCostOfAOrder;
            sp.setTotalSales(totalSalesOfSalesPerson);
            
          
        }
        topList=getTopThreeSalesPersonsbySalesVolume();
        System.out.println("Sales person list"+topList);
        firstTF.setText(topList.get(0).getName());
        secondTF.setText(topList.get(1).getName());
        thirdTF.setText(topList.get(2).getName());
        
    } 
//        for(Salesperson sp:salesPersonDirectory.getSalesPersonList())
//        {
//            if(sp.getTotalSales()>first)
//            {
//                first=sp.getTotalSales();
//                firstTF.setText(sp.getName());
//                
//            }
//            else if(sp.getTotalSales()>second)   
//            {
//                secondTF.setText(sp.getName());
//            }
//            else if(sp.getTotalSales()>third)
//            {
//                thirdTF.setText(sp.getName());
//            }
//        }
        
        
        public ArrayList<Salesperson> getTopThreeSalesPersonsbySalesVolume(){
         ArrayList<Salesperson> topThreeList = new ArrayList<>();
         
         
         double no1 = 0;
         double no2 = 0;
         double no3 = 0;
         
         Salesperson s1 = new Salesperson();
         Salesperson s2 = new Salesperson();
         Salesperson s3 = new Salesperson();
        
      //  Collections.sort(topThreeList, comparing());
        for(Salesperson s : salesPersonDirectory.getSalesPersonList()){
            double salesVolume = s.getTotalSales();
            
            if(salesVolume > no1){
                 no3 = no2;
                s3 = s2;
                no2 = no1;
                s2 = s1;
                
              no1= salesVolume;
            s1 = s;   
            }
            
            else if (salesVolume > no2){
              no3 = no2;
                s3 = s2;
                
              no2= salesVolume;
            s2 = s;  
                
            }
            else if (salesVolume >=no3){
                
              no3= salesVolume;
            s3 = s;  
                
                
            }
 
        }
            topThreeList.add(s1);
            topThreeList.add(s2);
            topThreeList.add(s3);
            System.out.println("the top three list is"+topThreeList);
        return topThreeList;
    }
        
        
    

    public void populateSalesPersonCombo()
    {
        
        salesPersonCombo.removeAllItems();

        for (Salesperson salesPerson : salesPersonDirectory.getSalesPersonList()) {

            salesPersonCombo.addItem(salesPerson);
        }
    }
    
    public void populateTable()
    {
    DefaultTableModel dtm = (DefaultTableModel) productTable.getModel();
      // sales = (Salesperson) salesPersonCombo.getSelectedItem();
        dtm.setRowCount(0);
            
            for (Order order:sales.getOrderCatalogue().getOrderList())
            {
                for(LineItem li:order.getLineItemList())
                {
                   Product p=new Product();
                   p=li.getProduct();
                   Object row[] = new Object[3];
                    row[0] = p;
                    row[1] = p.getModelNumber();
                    if(li.getActualPrice()<p.getTargetPrice())
                    {
                        p.setTargetStatus("Below");
                        countForBelow=countForBelow+1;
                    }
                    
                    else
                    {
                        p.setTargetStatus("Above");
                        countForAbove=countForAbove+1;
                    }
                    row[2] = p.getTargetStatus();
                    dtm.addRow(row);
                
                }
                
            }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        salesPersonCombo = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        productTable = new javax.swing.JTable();
        viewButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        aboveTF = new javax.swing.JTextField();
        belowTF = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        totalCommissionTF = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        secondTF = new javax.swing.JTextField();
        firstTF = new javax.swing.JTextField();
        thirdTF = new javax.swing.JTextField();

        jLabel1.setText("Salesperson Report");

        jLabel2.setText("Salesperson");

        salesPersonCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        salesPersonCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salesPersonComboActionPerformed(evt);
            }
        });

        productTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Product name", "Model Number", "Above/Below Target price"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(productTable);

        viewButton1.setText("View");
        viewButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewButton1ActionPerformed(evt);
            }
        });

        jLabel3.setText("Total number of products above sales volume:");

        jLabel4.setText("Total number of products below sales volume:");

        aboveTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboveTFActionPerformed(evt);
            }
        });

        jLabel5.setText("Total commission:");

        totalCommissionTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totalCommissionTFActionPerformed(evt);
            }
        });

        jLabel6.setText("Number 1 salesperson by sales volume:");

        jLabel7.setText("Number 2 salesperson by sales volume:");

        jLabel8.setText("Number 3 salesperson by sales volume:");

        secondTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                secondTFActionPerformed(evt);
            }
        });

        firstTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                firstTFActionPerformed(evt);
            }
        });

        thirdTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                thirdTFActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(95, 95, 95)
                        .addComponent(jLabel2)
                        .addGap(54, 54, 54)
                        .addComponent(salesPersonCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(154, 154, 154)
                        .addComponent(viewButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(aboveTF, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
                            .addComponent(belowTF)
                            .addComponent(totalCommissionTF)
                            .addComponent(thirdTF)
                            .addComponent(secondTF)
                            .addComponent(firstTF))))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(salesPersonCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(viewButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(aboveTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(belowTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(totalCommissionTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(firstTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(secondTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(thirdTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void salesPersonComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salesPersonComboActionPerformed
        // TODO add your handling code here:
                // TODO add your handling code here:
  
    }//GEN-LAST:event_salesPersonComboActionPerformed

    private void viewButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewButton1ActionPerformed

        // TODO add your handling code here:
      double commissionForEachOrder=0.0; 
      double totalCommissionOfAllOrders=0.0;
      sales = (Salesperson) salesPersonCombo.getSelectedItem();
      populateTable();
      aboveTF.setText(Integer.toString(countForAbove));
        belowTF.setText(Integer.toString(countForBelow));

        for(Order o:sales.getOrderCatalogue().getOrderList())
        {
            for(LineItem li:o.getLineItemList())
            {
                commissionForEachOrder=commissionForEachOrder+li.getComission();
            }
            totalCommissionOfAllOrders=totalCommissionOfAllOrders+commissionForEachOrder;
            o.setSalesVolume(totalCommissionOfAllOrders);
            
        }
        sales.setCommission(totalCommissionOfAllOrders);
        totalCommissionTF.setText(Double.toString(sales.getCommission()));
    }//GEN-LAST:event_viewButton1ActionPerformed

    private void aboveTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboveTFActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_aboveTFActionPerformed

    private void totalCommissionTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totalCommissionTFActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_totalCommissionTFActionPerformed

    private void secondTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_secondTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_secondTFActionPerformed

    private void firstTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_firstTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_firstTFActionPerformed

    private void thirdTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_thirdTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_thirdTFActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField aboveTF;
    private javax.swing.JTextField belowTF;
    private javax.swing.JTextField firstTF;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable productTable;
    private javax.swing.JComboBox salesPersonCombo;
    private javax.swing.JTextField secondTF;
    private javax.swing.JTextField thirdTF;
    private javax.swing.JTextField totalCommissionTF;
    private javax.swing.JButton viewButton1;
    // End of variables declaration//GEN-END:variables
}
