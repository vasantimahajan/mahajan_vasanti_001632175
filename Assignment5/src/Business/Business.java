/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Business {
    
    private CustomerDirectory customerDirectory;
    private SalesPersonDirectory salesPersonDirectory;
    private SupplierDirectory supplierDirectory;
    private CatalogueDirectory catalogueDirectory;
    private static Business business;
    private OrderCatalogue orderCatalogue;

    
    private Business()
    {
        customerDirectory=new CustomerDirectory();
        salesPersonDirectory=new SalesPersonDirectory();
        supplierDirectory=new SupplierDirectory();
        catalogueDirectory=new CatalogueDirectory();
        orderCatalogue=new OrderCatalogue();
    }
    
    public static Business getInstance()
    {
        if(business== null)
        {
            business=new Business();
        }
        return business;
    }

   

    public SalesPersonDirectory getSalesPersonDirectory() {
        return salesPersonDirectory;
    }

    public void setSalesPersonDirectory(SalesPersonDirectory salesPersonDirectory) {
        this.salesPersonDirectory = salesPersonDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public SupplierDirectory getSupplierDirectory() {
        return supplierDirectory;
    }

    public void setSupplierDirectory(SupplierDirectory supplierDirectory) {
        this.supplierDirectory = supplierDirectory;
    }

    public CatalogueDirectory getCatalogueDirectory() {
        return catalogueDirectory;
    }

    public void setCatalogueDirectory(CatalogueDirectory catalogueDirectory) {
        this.catalogueDirectory = catalogueDirectory;
    }

    public OrderCatalogue getOrderCatalogue() {
        return orderCatalogue;
    }

    public void setOrderCatalogue(OrderCatalogue orderCatalogue) {
        this.orderCatalogue = orderCatalogue;
    }
   
    
}
