/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Salesperson extends Person {
   
    private int salesPersonId;
    private double commission;
    private double totalSales;
    static int count=100;
    private OrderCatalogue orderCatalogue;
  public Salesperson()
    {
        salesPersonId=count++;
        orderCatalogue=new OrderCatalogue();
    }


    public int getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(int salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    @Override
    public String toString() {
        return Integer.toString(salesPersonId);
    }

    public OrderCatalogue getOrderCatalogue() {
        return orderCatalogue;
    }

    public void setOrderCatalogue(OrderCatalogue orderCatalogue) {
        this.orderCatalogue = orderCatalogue;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }
    
    
}
