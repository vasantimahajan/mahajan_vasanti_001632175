/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class Product {
    private String prodName;
    private double floorPrice;
    private double ceilingPrice;
    private double targetPrice;
    private double actualPrice;
    private double salesVolume;
    private ArrayList<LineItem>lineitemList;
    private int modelNumber;
    private int avail;
    private String targetStatus;
    
    private static int count =1000;

    @Override
    public String toString() {
        return prodName; 
    }

     public int getAvail() {
        return avail;
    }
    
    public void setAvail(int avail) {
        this.avail = avail;
    }
    
    public Product() {
    count++;
    modelNumber = count;
    lineitemList=new ArrayList<LineItem>();
    }
    public void addLineitem(LineItem li)
    {
        lineitemList.add(li);
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }


    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    public double getFloorPrice() {
        return floorPrice;
    }

    public void setFloorPrice(double floorPrice) {
        this.floorPrice = floorPrice;
    }

    public double getCeilingPrice() {
        return ceilingPrice;
    }

    public void setCeilingPrice(int ceilingPrice) {
        this.ceilingPrice = ceilingPrice;
    }

    public double getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(double targetPrice) {
        this.targetPrice = targetPrice;
    }

    public double getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(double actualPrice) {
        this.actualPrice = actualPrice;
    }

    public double getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(double salesVolume) {
        this.salesVolume = salesVolume;
    }

    public ArrayList<LineItem> getLineitemList() {
        return lineitemList;
    }

    public void setLineitemList(ArrayList<LineItem> lineitemList) {
        this.lineitemList = lineitemList;
    }

    public String getTargetStatus() {
        return targetStatus;
    }

    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }
    
    
    
}
