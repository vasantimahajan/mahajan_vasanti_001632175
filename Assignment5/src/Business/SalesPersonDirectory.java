/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class SalesPersonDirectory {
    private ArrayList<Salesperson> salesPersonList;
    
    SalesPersonDirectory()
    {
        salesPersonList=new ArrayList<Salesperson>();
    }

    public ArrayList<Salesperson> getSalesPersonList() {
        return salesPersonList;
    }

    public void setSalesPersonList(ArrayList<Salesperson> salesPersonList) {
        this.salesPersonList = salesPersonList;
    }
    
    public void addSalesPerson(String name)
    {
        Salesperson sales=new Salesperson();
        sales.setName(name);
        salesPersonList.add(sales);  
        
    }
    
    public void deleteSalesPerson(Salesperson s)
    {
        salesPersonList.remove(s);
    }
}
