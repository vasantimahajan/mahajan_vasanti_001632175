/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class Order {
    private static int count = 0;
    private ArrayList<LineItem> lineItemList;
    private int orderNumber;
    private Customer customer;
    private Salesperson salesperson;
    private double commission;

    private double salesVolume;
    @Override
    public String toString() {
        return Integer.toString(orderNumber);
    }
    
    public Order() {
        count++;
        orderNumber = count;
        lineItemList = new ArrayList<LineItem>();
        customer=new Customer();
        salesperson=new Salesperson();
    }
    
    public int getOrderNumber() {
        return orderNumber;
    }
    
    public void removeLineItem(LineItem o) {
        lineItemList.remove(o);
    }
    
    public LineItem addLineItem(Product p, int q, int price) {
        LineItem o = new LineItem();
        o.setProduct(p);
        o.setQuantity(q);
        o.setActualPrice(price);
        lineItemList.add(o);
        return o;
    }
    
    public ArrayList<LineItem> getLineItemList() {
        return lineItemList;
    }
    
    public void setOrderItemList(ArrayList<LineItem> lineItemList) {
        this.lineItemList = lineItemList;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Salesperson getSalesperson() {
        return salesperson;
    }

    public void setSalesperson(Salesperson salesperson) {
        this.salesperson = salesperson;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public double getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(double salesVolume) {
        this.salesVolume = salesVolume;
    }
    
    
}
