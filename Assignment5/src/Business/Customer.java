/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Customer extends Person{
    
    private int customerId;
    private double salesVolume;
    private OrderCatalogue orderCatalogue;

    @Override
    public String toString() {
        return Integer.toString(customerId);
    }
    private static int count =0;
    Customer()
    {
       count++;
       customerId = count;
       orderCatalogue=new OrderCatalogue();
    }

   
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }


    public double getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(double salesVolume) {
        this.salesVolume = salesVolume;
    }

    public OrderCatalogue getOrderCatalogue() {
        return orderCatalogue;
    }

    public void setOrderCatalogue(OrderCatalogue orderCatalogue) {
        this.orderCatalogue = orderCatalogue;
    }

    
    
}
