/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class SupplierDirectory {
    private ArrayList<Supplier> supplierList;
    
    
    public SupplierDirectory()
    {
        supplierList=new ArrayList<Supplier>();
       
    }

    public ArrayList<Supplier> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(ArrayList<Supplier> pharmacyList) {
        this.supplierList = supplierList;
    }
    
    public void addSupplier(String name)
    {
       Supplier supplier=new Supplier();
       supplier.setSupplierName(name);
       supplierList.add(supplier);  
       
    }
    
    public void deleteSupplier(Supplier s)
    {
        supplierList.remove(s);
    }
    
}
