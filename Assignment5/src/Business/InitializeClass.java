/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class InitializeClass {
    private SupplierDirectory supplierDirectory;
    
    public SupplierDirectory initializer(){
        
        supplierDirectory = new SupplierDirectory();
        supplierDirectory.addSupplier("Dell");
        supplierDirectory.addSupplier("HP");
        supplierDirectory.addSupplier("Toshiba");
        
        Supplier s1=supplierDirectory.getSupplierList().get(0);
        s1.getProductCatalog().addProducts("Dell_Laptop",100,200,150,20);
        s1.getProductCatalog().addProducts("Dell_Tablet",200,300,250,20);
        s1.getProductCatalog().addProducts("Dell_Phone",300,400,350,20);
        
        Supplier s2=supplierDirectory.getSupplierList().get(1);
        s2.getProductCatalog().addProducts("HP_Laptop",100,200,150,20);
        s2.getProductCatalog().addProducts("HP_Tablet",200,300,250,20);
        s2.getProductCatalog().addProducts("HP_Phone",300,400,350,20);
        
        Supplier s3=supplierDirectory.getSupplierList().get(2);
        s3.getProductCatalog().addProducts("Toshiba_Laptop",100,200,150,20);
        s3.getProductCatalog().addProducts("Toshiba_Tablet",200,300,250,20);
        s3.getProductCatalog().addProducts("Toshiba_Phone",300,400,350,20);
        
   
        for(Supplier s:supplierDirectory.getSupplierList())
        {
            System.out.println("Supplier name:"+s.getSupplierName());
             for(Product p: s.getProductCatalog().getProductList())
             {
                 System.out.println("The products in the catalogue are:"+p.getProdName()+" "+p.getFloorPrice()+" "+p.getCeilingPrice()+" "+p.getTargetPrice()+" "+p.getModelNumber()+" "+p.getAvail());
             }
        }  
        
    return  supplierDirectory;
    }    
}
