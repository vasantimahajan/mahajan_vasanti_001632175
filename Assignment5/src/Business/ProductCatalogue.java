/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class ProductCatalogue {
     private ArrayList<Product> productList;
    
    public ProductCatalogue()
    {
    productList=new ArrayList<Product>();
    }
    
   public void addProducts(String name,int floorPrice, int ceilingPrice, int targetPrice, int avail)
    {
        Product productObject=new Product();
        productObject.setProdName(name);
        productObject.setFloorPrice(floorPrice);
        productObject.setCeilingPrice(ceilingPrice);
        productObject.setTargetPrice(targetPrice);
        productObject.setAvail(avail);
        productList.add(productObject);
        
    }


    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productCatalogue) {
        this.productList = productCatalogue;
    }
    
}
