/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class UserAccount {
    private String username;
    private String password;
    private String role;
    private String isActive;
    private Person person; //the useraccount is associated with the person.
    public static final String ADMIN_ROLE="admin";// it is static because, 
    //it creates one value per class, not one value per instance, thus this is loaded
    //only once when the program is loaded not everytime when an instance is created.
    //it is final because its value is not going to change throughout the program
    //we can access the static variable from another class using the classname.variable
    public static final String EMPLOYEE_ROLE="employee";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
    
    
    
}
