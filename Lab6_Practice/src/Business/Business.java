/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Business {
    private UserAccountDirectory userAccountDir;
    private EmployeeDirectory empDir;
    private static Business business;// here the business object is created static since it is created only once
    
    private Business()//to use singleton pattern, i.e create only one instance 
            //of the business class, make the constructor private
    {
        userAccountDir=new UserAccountDirectory();
        empDir=new EmployeeDirectory();
    }
    
    public static Business getInstance()//ask why static?
    {
        if(business==null)// if the business object is created, do nothing,
            //but if it not created create it. Only once in the entire lifetime
            //of the project the object will be craeted.
        {
            business=new Business();
        }
        
        return business;
        
    }

    public UserAccountDirectory getUserAccountDir() {
        return userAccountDir;
    }

    public void setUserAccountDir(UserAccountDirectory userAccountDir) {
        this.userAccountDir = userAccountDir;
    }

    public EmployeeDirectory getEmpDir() {
        return empDir;
    }

    public void setEmpDir(EmployeeDirectory empDir) {
        this.empDir = empDir;
    }
    
    
    
}
