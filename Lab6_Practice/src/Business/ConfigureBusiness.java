/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class ConfigureBusiness {
    public static Business initializeBusiness()//create a default login of the admin
    {
        Business business=Business.getInstance();
        Employee e=business.getEmpDir().addEmployee();
        e.setFirstName("admin");
        e.setLastName("admin");
        e.setOrganization("NEU");
        UserAccount userAcc=business.getUserAccountDir().addUserAccount();
        userAcc.setUsername("admin");
        userAcc.setPassword("admin");
        userAcc.setRole(UserAccount.ADMIN_ROLE);//since the role was a static variable
        //we accessed it using the classname.variable
        userAcc.setIsActive("yes");
        userAcc.setPerson(e);
        return business;
    }
    
    
    
}
