/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class EmployeeDirectory {
    private ArrayList<Employee> employeeList;
    
    EmployeeDirectory()
    {
        employeeList=new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(ArrayList<Employee> employeeList) {
        this.employeeList = employeeList;
    }
    
    public Employee addEmployee()
    {
        Employee e=new Employee();
        employeeList.add(e);
        return e;
    }
    
    public void deleteEmployee(Employee e)
    {
        employeeList.remove(e);
    }
}
