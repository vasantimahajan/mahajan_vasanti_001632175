/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Person {
    private String firstName;
    private String lastName;
    private String middleName;
    private int dateOfBirthYYYY;
    private int dateOfBirthMM;
    private int dateOfBirthDD;
    private String streetAddress;
    private String town;
    private int zipcode;
    private String occupation;
    private String emailAddress;
    private int areaCode;
    private long phoneNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

   

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(int areaCode) {
        this.areaCode = areaCode;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getDateOfBirthYYYY() {
        return dateOfBirthYYYY;
    }

    public void setDateOfBirthYYYY(int dateOfBirthYYYY) {
        this.dateOfBirthYYYY = dateOfBirthYYYY;
    }

    public int getDateOfBirthMM() {
        return dateOfBirthMM;
    }

    public void setDateOfBirthMM(int dateOfBirthMM) {
        this.dateOfBirthMM = dateOfBirthMM;
    }

    public int getDateOfBirthDD() {
        return dateOfBirthDD;
    }

    public void setDateOfBirthDD(int dateOfBirthDD) {
        this.dateOfBirthDD = dateOfBirthDD;
    }
    
}
