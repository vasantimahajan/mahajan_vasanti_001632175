/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesss;

import Business.Organization.OrganizationDrectory;

/**
 *
 * @author Vasanti
 */
public class Business {
    
    private static Business business;
    private OrganizationDrectory organizationDirectory;
    
    public static Business getInstance()
    {
        if(business==null)
        {
            business=new Business();
        }
        
        return business;
     }
    private Business()
    {
        organizationDirectory=new OrganizationDrectory();
    }

    public OrganizationDrectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public void setOrganizationDirectory(OrganizationDrectory organizationDirectory) {
        this.organizationDirectory = organizationDirectory;
    }
    
    
}
