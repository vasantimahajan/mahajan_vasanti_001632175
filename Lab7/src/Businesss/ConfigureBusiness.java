/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Businesss;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Role.AdminRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author Vasanti
 */
public class ConfigureBusiness {
    public static Business initializeBusiness(){
       Business business=Business.getInstance();
       
       AdminOrganization adminOrg=new AdminOrganization();
       
       business.getOrganizationDirectory().getOrganizationList().add(adminOrg);
       
       Employee e =new Employee();
       e.setName("Vasanti");
        UserAccount account=new UserAccount();
        account.setUsername("admin");
        account.setPassword("admin");
        account.setRole(new AdminRole());
        account.setEmployee(e);
        
        adminOrg.getEmployeeDir().getEmployeeList().add(e);
        adminOrg.getUserAccDir().getUserAccList().add(account);
        return business;
       
    }
    
}
