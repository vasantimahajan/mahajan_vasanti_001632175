/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class OrganizationDrectory {
    private ArrayList<Organization> organizationList;
    
    public OrganizationDrectory()
    {
     organizationList=new ArrayList<>();
    }
    
    public Organization createOrg(Type type)
    {
        Organization org=null;
        if(type.getValue().equals(Type.Doctor.getValue()))
        {
            org=new DoctorOrganization();
            organizationList.add(org);
        }
        
        else if(type.getValue().equals(Type.Lab.getValue()))
        {
             org=new LabOrganization();
            organizationList.add(org);
        }
        
        return org;
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }

    public void setOrganizationList(ArrayList<Organization> organizationList) {
        this.organizationList = organizationList;
    }
            
    
}
