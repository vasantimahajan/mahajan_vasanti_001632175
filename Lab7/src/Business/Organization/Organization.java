/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public abstract class Organization {
    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDir;
    private UserAccountDirectory userAccDir;
    private int organizationId;
    private static int count;
    
    public Organization(String name)
    {
        this.name=name;
        this.workQueue=new WorkQueue();
        this.employeeDir=new EmployeeDirectory();
        this.userAccDir=new UserAccountDirectory();
        organizationId=count;
        count++;
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public EmployeeDirectory getEmployeeDir() {
        return employeeDir;
    }

    public void setEmployeeDir(EmployeeDirectory employeeDir) {
        this.employeeDir = employeeDir;
    }

    public UserAccountDirectory getUserAccDir() {
        return userAccDir;
    }

    public void setUserAccDir(UserAccountDirectory userAccDir) {
        this.userAccDir = userAccDir;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }
    public enum Type
    {
         Admin("Admin Organization")
        {
        
        }, Doctor ("Doctor organization"){

        }, Lab ("Lab organization")
        {
        
        };
        
        private String value;
        private Type(String value)
        {
            this.value=value;
        }
        
        public String getValue()
        {
            return value;
        }
        
       
       
    }
    @Override
    public String toString(){
        return name;
    }
       
    public abstract ArrayList<Role> getSupportedRoles();
    
    
}
