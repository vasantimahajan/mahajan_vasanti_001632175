/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Businesss.Business;
import javax.swing.JPanel;

/**
 *
 * @author Vasanti
 */
public abstract class Role {
    public abstract JPanel createWorkArea(JPanel upc, UserAccount us, Organization o,Business b);
    @Override
    public String toString(){
        return this.getClass().getName();
    }
}
