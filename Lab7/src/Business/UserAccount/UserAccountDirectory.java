/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.UserAccount;

import Business.Employee.Employee;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class UserAccountDirectory {
    private ArrayList<UserAccount> userAccList;
    
    public UserAccountDirectory()
        {
            userAccList=new ArrayList<UserAccount>();
        }

   
    public  UserAccount createUserAcc(String userName, String pass, Employee emp,Role role)
            
    {
        UserAccount u=new UserAccount();
        u.setUsername(userName);
        u.setPassword(pass);
        u.setRole(role);
        u.setEmployee(emp);
        userAccList.add(u);
        return u;
    }

    public ArrayList<UserAccount> getUserAccList() {
        return userAccList;
    }

    public void setUserAccList(ArrayList<UserAccount> userAccList) {
        this.userAccList = userAccList;
    }
    
    public UserAccount auenticateUser(String user, String pass)
    {
        for(UserAccount us:userAccList)
        {
            if(us.getUsername().equals(user)&& us.getPassword().equals(pass))
            {
                return us;
            }
        }
        
        return null;
    }
}
