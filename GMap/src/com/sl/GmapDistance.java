/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vasanti
 */
package com.sl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONObject;

public class GmapDistance {
  public static void main(String[] args) throws IOException {
	 String origin="44 Clearway Street";
         String destination="Northeastern University";
//	  URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins=44ClearwayStreet&destinations=NortheasternUniversity&key=AIzaSyAUftFKfNIO2RI64ZJM0joAG6Xtnolpc_8");
    URL url = new URL ("https://maps.googleapis.com/maps/api/geocode/json?address=1340+Boylston+Street,+Boston,+MA+02215&key=AIzaSyBYk0Mj7AKJD1lDBJlkRFAnjs16rz_JRJA");
         
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod("GET");
      String line, outputString = "";
      BufferedReader reader = new BufferedReader(
      new InputStreamReader(conn.getInputStream()));
      while ((line = reader.readLine()) != null) {
          outputString += line;
      }
      System.out.println("outputString >>>> "+outputString);
      try{
              JSONObject obj = new JSONObject(outputString); 
              JSONArray rows = obj.getJSONArray("results");
//String dist = obj.getJSONObject("rows").getJSONObject("elements").getString("distance");  
              
              for(int i = 0; i<rows.length();i++){
              JSONObject e=    rows.getJSONObject(i).getJSONObject("geometry");
              
              
                 double lat1= 42.3452983;
                 double lng1= -71.0850683;
                 
                 double lat2= 42.3440365;
                 double lng2= -71.098896;

              double result=HaversineInKM(lat1,lng1,lat2,lng2);
                  System.out.println("the distance is "+result);
              }

         // System.out.println("dist "+dist);
          
      }catch(Exception e){
          System.out.println("exc >> "+e.getMessage());
  }

      
      
      DistancePojo capRes = new Gson().fromJson(outputString, DistancePojo.class);
      System.out.println("capRes >> "+capRes);
  }
  
    static final double _eQuatorialEarthRadius = 6378.1370D;
    static final double _d2r = (Math.PI / 180D);

    public static int HaversineInM(double lat1, double long1, double lat2, double long2) {
        return (int) (1000D * HaversineInKM(lat1, long1, lat2, long2));
    }

    public static double HaversineInKM(double lat1, double long1, double lat2, double long2) {
        double dlong = (long2 - long1) * _d2r;
        double dlat = (lat2 - lat1) * _d2r;
        double a = Math.pow(Math.sin(dlat / 2D), 2D) + Math.cos(lat1 * _d2r) * Math.cos(lat2 * _d2r)
                * Math.pow(Math.sin(dlong / 2D), 2D);
        double c = 2D * Math.atan2(Math.sqrt(a), Math.sqrt(1D - a));
        double d = _eQuatorialEarthRadius * c;

        return d;
    }
}
