/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sl;

/**
 *
 * @author Vasanti
 */
import java.util.Arrays;

public class Rows {
	private Elements[] elements;

    public Elements[] getElements ()
    {
        return elements;
    }

    public void setElements (Elements[] elements)
    {
        this.elements = elements;
    }

	@Override
	public String toString() {
		return "Rows [elements=" + Arrays.toString(elements) + "]";
	}
    
}
