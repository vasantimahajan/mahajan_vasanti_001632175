/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skypecall;

import com.google.code.chatterbotapi.ChatterBot;
import com.google.code.chatterbotapi.ChatterBotFactory;
import com.google.code.chatterbotapi.ChatterBotSession;
import com.google.code.chatterbotapi.ChatterBotType;
import com.skype.Chat;
import com.skype.ChatMessage;
import com.skype.ChatMessageListener;
import com.skype.SkypeException;

/**
 *
 * @author Vasanti
 */
public class MyListener implements ChatMessageListener{

    private void myListener(String myMessage, Chat myChats) throws Exception
    {
        ChatterBotFactory myFactory=new ChatterBotFactory();
        ChatterBot skypeBot=myFactory.create(ChatterBotType.JABBERWACKY);
        ChatterBotSession skypeSession=skypeBot.createSession();
        
        try
        {
        myMessage=skypeSession.think(myMessage);
        final Chat chatterup=myChats;
        chatterup.send(myMessage);
                
        }
        
        catch(final SkypeException ex)
        {
            ex.printStackTrace();
        }
    }
    @Override
    public void chatMessageReceived(ChatMessage recMessage) throws SkypeException {
     try{
         myListener(recMessage.getContent(),recMessage.getChat());
         System.out.println(recMessage.getSenderDisplayName()+" - "+recMessage.getContent());
     }
     
     catch(final SkypeException ex)
     {
         ex.printStackTrace();
     }
     
     catch(Exception e)
     {
         e.printStackTrace();
         
     }
     }
    

    @Override
    public void chatMessageSent(ChatMessage sendMessage) throws SkypeException {
     try{
         System.out.println(sendMessage.getSenderDisplayName()+" - "+sendMessage.getContent());
     }
     
     catch(final SkypeException ex)
     {
         ex.printStackTrace();
     }
        
        
    }

    
}
