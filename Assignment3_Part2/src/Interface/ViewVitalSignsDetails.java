/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.Person;
import Business.VitalSigns;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

/**
 *
 * @author Vasanti
 */
public class ViewVitalSignsDetails extends javax.swing.JPanel {

    /**
     * Creates new form UpdateVitalSignsDetails
     */
    private Person person;
    private JPanel userProcessContainer;
    private boolean errorFlag;
    
    public ViewVitalSignsDetails(Person person, JPanel userProcessContainer) {
        initComponents();
        this.person=person;
        this.userProcessContainer=userProcessContainer;
        populateTable();
       
        nonreadableComponent();
        saveButton.setEnabled(false);
        respiratoryRateErrorMessage.setVisible(false);
        heartRateErrorMessage.setVisible(false);
        systolicBloodPressureErrorMessage.setVisible(false);
        weightErrorMessage.setVisible(false);
        updateVitalSignsButton.setEnabled(true);
                       
    }
    
    public void populateTable()
    {
    
     DefaultTableModel table = (DefaultTableModel)viewVitalSignsTable.getModel();
        int rowCount = viewVitalSignsTable.getRowCount();
        for(int i = rowCount - 1; i>=0; i--)
        {
            table.removeRow(i);
        }
        
        for(VitalSigns vs: person.getPatientDetails().getVitalSignsHistory().getVitalSignList())
        {
            Object row[] = new Object[6];
            row[0] = vs;
            row[1] = vs.getHeartRate();
            row[2] = vs.getSystolicBloodPressure();
            row[3] = vs.getWeight();
            row[4] = vs.getTimeStamp();
                                     
            boolean checkState;
            checkState=person.getPatientDetails().getVitalSignsHistory().checkVitalSignState(person,vs);
            
            if(checkState==true)
            {
                row[5] = "Normal";
            }
            
            else
            {
                row[5]="Abnormal";        
             
            }
            table.addRow(row);
            
            
        }
    
    }
    
   
    public void displayComponents()
    {
        respiratoryRateTextField.setVisible(false);
        heartRateTextField.setVisible(false);
        systolicBloodPressureTextField.setVisible(false);
        weightTextField.setVisible(false);
        respiratoryRateLabel.setVisible(false);
        heartRateLabel.setVisible(false);
        systolicBloodPressureLabel.setVisible(false);
        weightLabel.setVisible(false);
        respiratoryRateErrorMessage.setVisible(false);
        heartRateErrorMessage.setVisible(false);
        systolicBloodPressureErrorMessage.setVisible(false);
        weightErrorMessage.setVisible(false);
        updateButton.setVisible(false);
        saveButton.setVisible(false);
    }

    public void enableComponents()
    {
        respiratoryRateTextField.setVisible(true);
        heartRateTextField.setVisible(true);
        systolicBloodPressureTextField.setVisible(true);
        weightTextField.setVisible(true);
        respiratoryRateLabel.setVisible(true);
        heartRateLabel.setVisible(true);
        systolicBloodPressureLabel.setVisible(true);
        weightLabel.setVisible(true);
        respiratoryRateErrorMessage.setVisible(true);
        heartRateErrorMessage.setVisible(true);
        systolicBloodPressureErrorMessage.setVisible(true);
        weightErrorMessage.setVisible(true);
        updateButton.setVisible(true);
        updateVitalSignsButton.setVisible(true);
        saveButton.setVisible(true);
        
        
    }
   
    public void nonreadableComponent()
           
            {
              respiratoryRateTextField.setEnabled(false);
        heartRateTextField.setEnabled(false);
        systolicBloodPressureTextField.setEnabled(false);
        weightTextField.setEnabled(false);
        respiratoryRateLabel.setEnabled(false);
        heartRateLabel.setEnabled(false);
        systolicBloodPressureLabel.setEnabled(false);
        weightLabel.setEnabled(false);
        respiratoryRateErrorMessage.setEnabled(false);
        heartRateErrorMessage.setEnabled(false);
        systolicBloodPressureErrorMessage.setEnabled(false);
        weightErrorMessage.setEnabled(false);
         updateButton.setEnabled(false);
         updateVitalSignsButton.setEnabled(false);
         
            }
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        heartRateErrorMessage = new javax.swing.JLabel();
        heartRateTextField = new javax.swing.JTextField();
        systolicBloodPressureErrorMessage = new javax.swing.JLabel();
        systolicBloodPressureTextField = new javax.swing.JTextField();
        weightErrorMessage = new javax.swing.JLabel();
        weightTextField = new javax.swing.JTextField();
        updateButton = new javax.swing.JButton();
        respiratoryRateLabel = new javax.swing.JLabel();
        heartRateLabel = new javax.swing.JLabel();
        systolicBloodPressureLabel = new javax.swing.JLabel();
        weightLabel = new javax.swing.JLabel();
        respiratoryRateTextField = new javax.swing.JTextField();
        respiratoryRateErrorMessage = new javax.swing.JLabel();
        addVitalSignsHeader = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        viewVitalSignsTable = new javax.swing.JTable();
        updateVitalSignsButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        generateReport = new javax.swing.JButton();
        viewButton = new javax.swing.JButton();

        heartRateErrorMessage.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        heartRateErrorMessage.setForeground(new java.awt.Color(255, 0, 0));

        systolicBloodPressureErrorMessage.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        systolicBloodPressureErrorMessage.setForeground(new java.awt.Color(255, 0, 0));

        weightErrorMessage.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        weightErrorMessage.setForeground(new java.awt.Color(255, 0, 0));

        updateButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        updateButton.setText("Update");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        respiratoryRateLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        respiratoryRateLabel.setText("Respiratory rate:");

        heartRateLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        heartRateLabel.setText("Heart rate:");

        systolicBloodPressureLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        systolicBloodPressureLabel.setText("Systolic blood pressure:");

        weightLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        weightLabel.setText("Weight (in lbs):");

        respiratoryRateTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                respiratoryRateTextFieldActionPerformed(evt);
            }
        });

        respiratoryRateErrorMessage.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        respiratoryRateErrorMessage.setForeground(new java.awt.Color(255, 0, 0));

        addVitalSignsHeader.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        addVitalSignsHeader.setText("View Vital Signs");

        viewVitalSignsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Respiratory Rate", "Heart Rate", "Systolic blood pressure", "Weight", "Date", "Normality"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(viewVitalSignsTable);

        updateVitalSignsButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        updateVitalSignsButton.setText("Update Vital Signs");
        updateVitalSignsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateVitalSignsButtonActionPerformed(evt);
            }
        });

        backButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        backButton.setText("Back");
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        saveButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        saveButton.setText("Save");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });

        generateReport.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        generateReport.setText("Generate Vital Signs Report");
        generateReport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateReportActionPerformed(evt);
            }
        });

        viewButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        viewButton.setText("View abnormal parameter");
        viewButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 532, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(updateVitalSignsButton)
                                .addGap(18, 18, 18)
                                .addComponent(viewButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(generateReport))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(88, 88, 88)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(respiratoryRateLabel)
                                    .addComponent(heartRateLabel)
                                    .addComponent(systolicBloodPressureLabel)
                                    .addComponent(weightLabel))
                                .addGap(44, 44, 44)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(respiratoryRateErrorMessage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(weightErrorMessage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(respiratoryRateTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(heartRateTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(heartRateErrorMessage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(systolicBloodPressureTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(systolicBloodPressureErrorMessage, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(weightTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(backButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(updateButton)
                                .addGap(31, 31, 31)
                                .addComponent(saveButton)
                                .addGap(64, 64, 64))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(253, 253, 253)
                        .addComponent(addVitalSignsHeader)))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(addVitalSignsHeader)
                .addGap(15, 15, 15)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(generateReport)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(updateVitalSignsButton)
                        .addComponent(viewButton)))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(respiratoryRateLabel)
                    .addComponent(respiratoryRateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addComponent(respiratoryRateErrorMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(heartRateTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(heartRateLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(heartRateErrorMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(systolicBloodPressureLabel)
                    .addComponent(systolicBloodPressureTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(systolicBloodPressureErrorMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(weightLabel)
                    .addComponent(weightTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(weightErrorMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateButton)
                    .addComponent(backButton)
                    .addComponent(saveButton))
                .addContainerGap(54, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        // TODO add your handling code here:
        enableComponents();
        respiratoryRateTextField.setEnabled(true);
        heartRateTextField.setEnabled(true);
        weightTextField.setEnabled(true);
        systolicBloodPressureTextField.setEnabled(true);
        updateVitalSignsButton.setVisible(false);
        updateButton.setVisible(false);
        saveButton.setEnabled(true);
        respiratoryRateErrorMessage.setEnabled(true);
        heartRateErrorMessage.setEnabled(true);
        weightErrorMessage.setEnabled(true);
        systolicBloodPressureErrorMessage.setEnabled(true);
        respiratoryRateLabel.setEnabled(true);
        heartRateLabel.setEnabled(true);
        systolicBloodPressureLabel.setEnabled(true);
        weightLabel.setEnabled(true);
        
        
        
        
    }//GEN-LAST:event_updateButtonActionPerformed

    private void respiratoryRateTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_respiratoryRateTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_respiratoryRateTextFieldActionPerformed
    private VitalSigns vs;
    private void updateVitalSignsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateVitalSignsButtonActionPerformed
        // TODO add your handling code here:
        int rowSelected=viewVitalSignsTable.getSelectedRow();
        if(rowSelected>=0)
        {
            vs=(VitalSigns) viewVitalSignsTable.getValueAt(rowSelected,0);
            enableComponents();
           
            respiratoryRateTextField.setText(Integer.toString(vs.getRespiratoryRate()));
            respiratoryRateTextField.setEnabled(false);
            heartRateTextField.setText(Integer.toString(vs.getHeartRate()));
            heartRateTextField.setEnabled(false);
            systolicBloodPressureTextField.setText(Integer.toString(vs.getSystolicBloodPressure()));
            systolicBloodPressureTextField.setEnabled(false);
            weightTextField.setText(Integer.toString(vs.getWeight()));
            weightTextField.setEnabled(false);
            saveButton.setEnabled(false);
            updateVitalSignsButton.setVisible(false);
            generateReport.setVisible(false);
            updateButton.setEnabled(true);
            viewButton.setVisible(false);
            respiratoryRateErrorMessage.setVisible(false);
            heartRateErrorMessage.setVisible(false);
            systolicBloodPressureErrorMessage.setVisible(false);
            weightErrorMessage.setVisible(false);
            
        }
        
        else
        {
         JOptionPane.showMessageDialog(null, "Please choose a row from the table to update", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
        
        
        
    }//GEN-LAST:event_updateVitalSignsButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // TODO add your handling code here:
               boolean check=validateFields();
                if(check==true)
        {
                vs.setRespiratoryRate(Integer.parseInt(respiratoryRateTextField.getText()));
                vs.setHeartRate(Integer.parseInt(heartRateTextField.getText()));
                vs.setSystolicBloodPressure(Integer.parseInt(systolicBloodPressureTextField.getText()));
                vs.setWeight(Integer.parseInt(weightTextField.getText()));
                populateTable();
                nonreadableComponent();
                saveButton.setEnabled(false);
                updateButton.setEnabled(false);
                updateVitalSignsButton.setEnabled(true);
                updateButton.setVisible(true);
                updateVitalSignsButton.setVisible(true);
                generateReport.setVisible(true);
                viewButton.setVisible(true);
        }

    }//GEN-LAST:event_saveButtonActionPerformed

    public boolean validateFields()
    {
            errorFlag=true;
        if(respiratoryRateTextField.getText().equals(""))
        {
           
            errorFlag=false;
            respiratoryRateErrorMessage.setText("Please enter the respiratory rate");
        }
        else
        {
           
            respiratoryRateErrorMessage.setText("");
        }
        
         if(heartRateTextField.getText().equals(""))
        {
           
            errorFlag=false;
            heartRateErrorMessage.setText("Please enter the heart rate");
        }
        else
        {
           
            heartRateErrorMessage.setText("");
        }
          if(systolicBloodPressureTextField.getText().equals(""))
        {
           
            errorFlag=false;
            systolicBloodPressureErrorMessage.setText("Please enter the blood pressure");
        }
        else
        {
           
            systolicBloodPressureErrorMessage.setText("");
        }
          
           if(weightTextField.getText().equals(""))
        {
           
            errorFlag=false;
            weightErrorMessage.setText("Please enter the weight");
        }
        else
        {
           
            weightErrorMessage.setText("");
        }
           return errorFlag;
    }
    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backButtonActionPerformed

    private void generateReportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateReportActionPerformed
        // TODO add your handling code here:
        JPanel panel = new JPanel();
        
        XYDataset ds = createDataset();
        JFreeChart chart = ChartFactory.createXYLineChart("Vital Signs Chart",
                "x", "y", ds, PlotOrientation.VERTICAL, true, true,
                false);
                
       ChartFrame frame = new ChartFrame("charts", chart);
        
        frame.setSize(600, 400);
      
        frame.setVisible(true);

    
    }//GEN-LAST:event_generateReportActionPerformed

    private void viewButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewButtonActionPerformed
        // TODO add your handling code here:
        int rowSelected=viewVitalSignsTable.getSelectedRow();
        if(rowSelected>=0)
        {
             vs=(VitalSigns) viewVitalSignsTable.getValueAt(rowSelected,0);
             respiratoryRateTextField.setText(Integer.toString(vs.getRespiratoryRate()));
             heartRateTextField.setText(Integer.toString(vs.getHeartRate()));
             systolicBloodPressureTextField.setText(Integer.toString(vs.getSystolicBloodPressure()));
             weightTextField.setText(Integer.toString(vs.getWeight()));
             
             respiratoryRateTextField.setEnabled(false);
             heartRateTextField.setEnabled(false);
             weightTextField.setEnabled(false);
             systolicBloodPressureTextField.setEnabled(false);
             respiratoryRateErrorMessage.setEnabled(true);
             heartRateErrorMessage.setEnabled(true);
             weightErrorMessage.setEnabled(true);
             systolicBloodPressureErrorMessage.setEnabled(true);
             respiratoryRateErrorMessage.setVisible(true);
             heartRateErrorMessage.setVisible(true);
             weightErrorMessage.setVisible(true);
             systolicBloodPressureErrorMessage.setVisible(true);
             updateButton.setVisible(false);
             saveButton.setVisible(false);
             respiratoryRateLabel.setEnabled(true);
             heartRateLabel.setEnabled(true);
             systolicBloodPressureLabel.setEnabled(true);
             weightLabel.setEnabled(true);
             
             if(vs.getRespiratoryRateStatus().equals("abnormal"))
            {
                
              respiratoryRateErrorMessage.setText("Respiratory rate is abnormal");
            }
            else
            {
            respiratoryRateErrorMessage.setText("");
            }
            if(vs.getHeartRateStatus().equals("abnormal"))
            {
                heartRateErrorMessage.setText("Heart rate is abnormal");
            }
            
            else
            {
             heartRateErrorMessage.setText("");
            }
            if(vs.getSystolicBloodPressureStatus().equals("abnormal"))
            {
                systolicBloodPressureErrorMessage.setText(" Systolic pressue is abnormal");
                
            }
            
            else
            {
            systolicBloodPressureErrorMessage.setText("");
            }
            if(vs.getWeightStatus().equals("abnormal"))
            {
                weightErrorMessage.setText("Weight is abnormal");
            }
            else
            {
             weightErrorMessage.setText("");
            }
        }
        
         else
        {
         JOptionPane.showMessageDialog(null, "Please choose a row from the table to view", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
        
    }//GEN-LAST:event_viewButtonActionPerformed

     private XYDataset createDataset() {

        DefaultXYDataset ds = new DefaultXYDataset();
        ArrayList<VitalSigns> list = person.getPatientDetails().getVitalSignsHistory().getVitalSignList();
        double[] dataBloodPressure = new double[person.getPatientDetails().getVitalSignsHistory().getVitalSignList().size()];
        double[] dataHeartRate = new double[person.getPatientDetails().getVitalSignsHistory().getVitalSignList().size()];
        double[] dataRespiratoryRate = new double[person.getPatientDetails().getVitalSignsHistory().getVitalSignList().size()];
        double[] dataWeight = new double[person.getPatientDetails().getVitalSignsHistory().getVitalSignList().size()];
        double[] dataDate = new double[person.getPatientDetails().getVitalSignsHistory().getVitalSignList().size()];
        for (int i = 0; i < list.size(); i++) {
            dataBloodPressure[i] = list.get(i).getSystolicBloodPressure();
            dataHeartRate[i] = list.get(i).getHeartRate();
            dataRespiratoryRate[i] = list.get(i).getRespiratoryRate();
            dataWeight[i] = list.get(i).getWeight();
            dataDate[i] = i;

        }
        double[][] bloodPressureArray = {dataDate, dataBloodPressure};
        double[][] heartRateArray = {dataDate, dataHeartRate};
        double[][] respiratoryRateArray = {dataDate, dataRespiratoryRate};
        double[][] weightArray = {dataDate, dataWeight};
        ds.addSeries("Blood Pressure", bloodPressureArray);
        ds.addSeries("Heart Rate", heartRateArray);
        ds.addSeries("Respiratory Rate", respiratoryRateArray);
        ds.addSeries("Weight", weightArray);

        return ds;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel addVitalSignsHeader;
    private javax.swing.JButton backButton;
    private javax.swing.JButton generateReport;
    private javax.swing.JLabel heartRateErrorMessage;
    private javax.swing.JLabel heartRateLabel;
    private javax.swing.JTextField heartRateTextField;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel respiratoryRateErrorMessage;
    private javax.swing.JLabel respiratoryRateLabel;
    private javax.swing.JTextField respiratoryRateTextField;
    private javax.swing.JButton saveButton;
    private javax.swing.JLabel systolicBloodPressureErrorMessage;
    private javax.swing.JLabel systolicBloodPressureLabel;
    private javax.swing.JTextField systolicBloodPressureTextField;
    private javax.swing.JButton updateButton;
    private javax.swing.JButton updateVitalSignsButton;
    private javax.swing.JButton viewButton;
    private javax.swing.JTable viewVitalSignsTable;
    private javax.swing.JLabel weightErrorMessage;
    private javax.swing.JLabel weightLabel;
    private javax.swing.JTextField weightTextField;
    // End of variables declaration//GEN-END:variables
}
