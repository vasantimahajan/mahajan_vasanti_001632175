/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import com.sun.jmx.snmp.BerDecoder;
import java.util.Date;

/**
 *
 * @author Vasanti
 */
public class VitalSigns {
    private int respiratoryRate;
    private int heartRate;
    private int systolicBloodPressure;
    private int weight;
    private Date timestamp;
    private String normality;
    private String respiratoryRateStatus;
    private String heartRateStatus;
    private String weightStatus;
    private String systolicBloodPressureStatus;
    
 

    public int getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(int respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(int systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    
    @Override
    public String toString() {
        return Integer.toString(respiratoryRate);
    }

    public Date getTimeStamp() {
        return timestamp;
    }

    public void setTimeStamp(Date timeString) {
        this.timestamp = timeString;
    }

    public String getNormality() {
        return normality;
    }

    public void setNormality(String normality) {
        this.normality = normality;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getRespiratoryRateStatus() {
        return respiratoryRateStatus;
    }

    public void setRespiratoryRateStatus(String respiratoryRateStatus) {
        this.respiratoryRateStatus = respiratoryRateStatus;
    }

    public String getHeartRateStatus() {
        return heartRateStatus;
    }

    public void setHeartRateStatus(String heartRateStatus) {
        this.heartRateStatus = heartRateStatus;
    }

    public String getWeightStatus() {
        return weightStatus;
    }

    public void setWeightStatus(String weightStatus) {
        this.weightStatus = weightStatus;
    }

    public String getSystolicBloodPressureStatus() {
        return systolicBloodPressureStatus;
    }

    public void setSystolicBloodPressureStatus(String systolicBloodPressureStatus) {
        this.systolicBloodPressureStatus = systolicBloodPressureStatus;
    }
    
    
    
            
}
