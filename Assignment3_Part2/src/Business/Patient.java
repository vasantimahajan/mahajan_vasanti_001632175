


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class Patient {
    
    private String patientName;
    private int patientId;
    private int age;
    private String primaryDoctorsName;
    private String preferredPharmacy;
    private VitalSignsHistory vitalSignsHistory;
    private String normality;
    
    public Patient()
    {
        vitalSignsHistory=new VitalSignsHistory();
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getPrimaryDoctorsName() {
        return primaryDoctorsName;
    }

    public void setPrimaryDoctorsName(String primaryDoctorsName) {
        this.primaryDoctorsName = primaryDoctorsName;
    }

    public String getPreferredPharmacy() {
        return preferredPharmacy;
    }

    public void setPreferredPharmacy(String preferredPharmacy) {
        this.preferredPharmacy = preferredPharmacy;
    }
    
   
    public VitalSignsHistory getVitalSignsHistory() {
        return vitalSignsHistory;
    }

    public void setVitalSignsHistory(VitalSignsHistory vitalSignsHistory) {
        this.vitalSignsHistory = vitalSignsHistory;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return patientName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNormality() {
        return normality;
    }

    public void setNormality(String normality) {
        this.normality = normality;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }
}
