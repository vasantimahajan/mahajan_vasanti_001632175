/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */

public class PersonList {
   private ArrayList<Person> personList=new ArrayList<>();
      
   public Person addPerson()
   {
       Person person=new Person();
       personList.add(person);
       return person;
       
   }
   
   public void deletePerson(Person p)
   {
       personList.remove(p);
   }

    public ArrayList<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }
}
