/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Person {
    
    private String name;
    private int id;
    private String address;
    private Patient patientDetails;
    private boolean patientCreated;
    private static int count=0;
    
    public Person()
    {
        patientDetails=new Patient();
        count++;
        id=count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Patient getPatientDetails() {
        return patientDetails;
    }

    public void setPatientDetails(Patient patientDetails) {
        this.patientDetails = patientDetails;
    }
    
    @Override
    public String toString() {
        return this.name;
    }

    public boolean getPatientCreated() {
        return patientCreated;
    }

    public void setPatientCreated(boolean patientCreated) {
        this.patientCreated = patientCreated;
    }
    
}
