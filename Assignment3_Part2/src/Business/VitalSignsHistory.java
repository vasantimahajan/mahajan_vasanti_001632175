/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Vasanti
 */

public class VitalSignsHistory {
    
     private ArrayList<VitalSigns> vitalSignList=new ArrayList<>();
    
     
      public ArrayList<VitalSigns> getVitalSignList() {
        return vitalSignList;
    }

    public void setVitalSignList(ArrayList<VitalSigns> vitalSignList) {
        this.vitalSignList = vitalSignList;
    }
    
    public VitalSigns addVitalSign()
    {
    VitalSigns vitalSign= new VitalSigns();
    vitalSignList.add(vitalSign);
    return vitalSign;
    }
    
     public boolean checkVitalSignState(Person person, VitalSigns vitalSigns)
    {
        boolean check=true;
        String ageGroup = "null";
        int age=person.getPatientDetails().getAge();
        //System.out.println("age" + age);
        switch (age)
        {
        case 1: case 2: case 3:
                ageGroup="toddler";
                break;
        case 4: case 5:
                ageGroup="preSchooler";
                break;
        case 6: case 7: case 8: case 9: case 10: case 11: case 12:
                ageGroup="schoolAge";
                break;
                
        default:
                ageGroup="adolescent";
                break;
        }
        
        if(ageGroup.equals("toddler"))
        {
            if((vitalSigns.getRespiratoryRate()>=20)&&(vitalSigns.getRespiratoryRate()<=30))
            { 
                vitalSigns.setRespiratoryRateStatus("normal");
            }  
            else
            {
                 vitalSigns.setRespiratoryRateStatus("abnormal");
                 check=false;
            }
             
            if((vitalSigns.getHeartRate()>=80)&&(vitalSigns.getHeartRate()<=130))
            {
                vitalSigns.setHeartRateStatus("normal");
            }  
            else
            {
                vitalSigns.setHeartRateStatus("abnormal");
                check=false;
            }  
             
            
            if((vitalSigns.getSystolicBloodPressure()>=80)&&(vitalSigns.getSystolicBloodPressure()<=110))
               {
               vitalSigns.setSystolicBloodPressureStatus("normal");
            }  
            else
            {
               vitalSigns.setSystolicBloodPressureStatus("abnormal");
               check=false;
            }  
            
            if((vitalSigns.getWeight()>=22)&&(vitalSigns.getWeight()<=31))
            {
            vitalSigns.setWeightStatus("normal");
            }  
            else
            {
                vitalSigns.setWeightStatus("abnormal");
                check=false;
            }  
                    
        }
            
            
        
        if(ageGroup.equals("preSchooler"))
        {
            if((vitalSigns.getRespiratoryRate()>=20)&&(vitalSigns.getRespiratoryRate()<=30))
                { 
                vitalSigns.setRespiratoryRateStatus("normal");
                
                }  
            else
            {
                vitalSigns.setRespiratoryRateStatus("abnormal");
                check=false;
            }
            
            if((vitalSigns.getHeartRate()>=80)&&(vitalSigns.getHeartRate()<=120))
            
            {
               vitalSigns.setHeartRateStatus("normal");
            }  
            else
            {
                vitalSigns.setHeartRateStatus("abnormal");
                check=false;
            } 
            
            if((vitalSigns.getSystolicBloodPressure()>=80)&&(vitalSigns.getSystolicBloodPressure()<=110))
                {
                  vitalSigns.setSystolicBloodPressureStatus("normal");
            }  
            else
            {
                 vitalSigns.setSystolicBloodPressureStatus("abnormal");
                 check=false;
            }  
            
            if((vitalSigns.getWeight()>=31)&&(vitalSigns.getWeight()<=40))
            {
                 vitalSigns.setWeightStatus("normal");
            }  
            else
            {
                vitalSigns.setWeightStatus("abnormal");
                check=false;
            }
            
        }   
          
        
         if(ageGroup.equals("schoolAge"))
        {
            if((vitalSigns.getRespiratoryRate()>=20)&&(vitalSigns.getRespiratoryRate()<=30))
             { 
               vitalSigns.setRespiratoryRateStatus("normal");
             }  
            else
            {
               vitalSigns.setRespiratoryRateStatus("abnormal");
               check=false;
            }
            
            if((vitalSigns.getHeartRate()>=70)&&(vitalSigns.getHeartRate()<=110))
            {
               vitalSigns.setHeartRateStatus("normal");
            }  
            else
            {
                vitalSigns.setHeartRateStatus("abnormal");
                check=false;
            } 
            
            if((vitalSigns.getSystolicBloodPressure()>=80)&&(vitalSigns.getSystolicBloodPressure()<=120))
            {
           vitalSigns.setSystolicBloodPressureStatus("normal");
            }  
            else
            {
                 vitalSigns.setSystolicBloodPressureStatus("abnormal");
                 check=false;
            }  
                    
            if((vitalSigns.getWeight()>=41)&&(vitalSigns.getWeight()<=92))
            {
               vitalSigns.setWeightStatus("normal");
            }  
            else
            {
                vitalSigns.setWeightStatus("abnormal");
                check=false;
            }
        }
         
          
         if(ageGroup.equals("adolescent"))
        {
            if((vitalSigns.getRespiratoryRate()>=12)&&(vitalSigns.getRespiratoryRate()<=20))
               { 
                vitalSigns.setRespiratoryRateStatus("normal");
             }  
            else
            {
                vitalSigns.setRespiratoryRateStatus("abnormal");
                check=false;
            }
            
             if((vitalSigns.getHeartRate()>=55)&&(vitalSigns.getHeartRate()<=105))
                    {
               vitalSigns.setHeartRateStatus("normal");
            }  
            else
            {
               vitalSigns.setHeartRateStatus("abnormal");
               check=false;
            } 
             
             
             if((vitalSigns.getSystolicBloodPressure()>=110)&&(vitalSigns.getSystolicBloodPressure()<=120))
                  {
           vitalSigns.setSystolicBloodPressureStatus("normal");
            }  
            else
            {
                 vitalSigns.setSystolicBloodPressureStatus("abnormal");
                 check=false;
            }    
             
            if((vitalSigns.getWeight()>=110))
            {
               vitalSigns.setWeightStatus("normal");
            }  
            else
            {
                vitalSigns.setWeightStatus("abnormal");
                check=false;
            }
        }
       
        return check;  
        }
            
        
            
        
                    
                    
        
        
       
        
        
    
       
//        if(ageGroup.equals("toddler"))
//        {
//            if((vitalSigns.getRespiratoryRate()>=20)&&(vitalSigns.getRespiratoryRate()<=30)&&(vitalSigns.getHeartRate()>=80)&&(vitalSigns.getHeartRate()<=130)&&(vitalSigns.getSystolicBloodPressure()>=80)&&(vitalSigns.getSystolicBloodPressure()<=110)&&(vitalSigns.getWeight()>=22)&&(vitalSigns.getWeight()<=31))
//            {
//                System.out.println("The toddler patient has Normal Vital Signs");
//            }
//            
//            else
//            {
//                check=false;
//            }
//        }
//        
//        else if(ageGroup.equals("preSchooler"))
//        {
//            if((vitalSigns.getRespiratoryRate()>=20)&&(vitalSigns.getRespiratoryRate()<=30)&&(vitalSigns.getHeartRate()>=80)&&(vitalSigns.getHeartRate()<=120)&&(vitalSigns.getSystolicBloodPressure()>=80)&&(vitalSigns.getSystolicBloodPressure()<=110)&&(vitalSigns.getWeight()>=31)&&(vitalSigns.getWeight()<=40))
//            {
//                System.out.println("The pre schooler patient has Normal Vital Signs");
//            }
//            
//            else
//            {
//                check=false;
//            }
//        }
//        
//        else if(ageGroup.equals("schoolAge"))
//        {
//            if((vitalSigns.getRespiratoryRate()>=20)&&(vitalSigns.getRespiratoryRate()<=30)&&(vitalSigns.getHeartRate()>=70)&&(vitalSigns.getHeartRate()<=110)&&(vitalSigns.getSystolicBloodPressure()>=80)&&(vitalSigns.getSystolicBloodPressure()<=120)&&(vitalSigns.getWeight()>=41)&&(vitalSigns.getWeight()<=92))
//            {
//                System.out.println("The school age patient has Normal Vital Signs");
//            }
//            
//            else
//            {
//                check=false;
//            }
//        }
//        
//        else if(ageGroup.equals("adolescent"))
//        {
//            if((vitalSigns.getRespiratoryRate()>=12)&&(vitalSigns.getRespiratoryRate()<=20)&&(vitalSigns.getHeartRate()>=55)&&(vitalSigns.getHeartRate()<=105)&&(vitalSigns.getSystolicBloodPressure()>=110)&&(vitalSigns.getSystolicBloodPressure()<=120)&&(vitalSigns.getWeight()>=110))
//            {
//                System.out.println("The adolescent patient has Normal Vital Signs");
//            }
//            
//            else
//            {
//                check=false;
//            }
//        }
//        else
//        {
//            check = false;
//        }
//        return check;  
//    }
    
     public void deleteVitalSign(VitalSigns vitalSigns)
     {
      vitalSignList.remove(vitalSigns);
     }
    
}
