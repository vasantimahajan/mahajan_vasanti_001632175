/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Messaging;

import com.sun.xml.internal.ws.wsdl.writer.document.Message;
import hu.ozeki.OzSMSMessage;
import hu.ozeki.OzSmsClient;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author Vasanti
 */
public class MyOzSmsClient extends OzSmsClient{
    
    
public MyOzSmsClient(String host, int port) throws IOException,
   InterruptedException {
 super(host, port);
}

    @Override
    public void doOnMessageReceived(OzSMSMessage sms) {
       Date now = new Date();
 System.out.println(now.toString() + " Message received. Senderaddress: " + sms.sender + " Message text: " + sms.messageData);
        
        
    }

    @Override
    public void doOnMessageDeliveredToHandset(OzSMSMessage sms) {
        Date now = new Date();
 System.out.println(now.toString() + " Message delivered to handset. ID: " + sms.messageId);
    }

    @Override
    public void doOnMessageDeliveredToNetwork(OzSMSMessage sms) {
      
    Date now = new Date();
 System.out.println(now.toString() + " Message delivered to network. ID: " + sms.messageId);
    }
    @Override
    public void doOnMessageAcceptedForDelivery(OzSMSMessage sms) {
         Date now = new Date();
 System.out.println(now.toString() + " Message accepted for delivery. ID: " + sms.messageId);
    }

    @Override
    public void doOnMessageDeliveryError(OzSMSMessage sms) {
       Date now = new Date();
 System.out.println(now.toString() + " Message could not be delivered. ID: " + sms.messageId +" Error message: " + sms.errorMessage + "\r\n");
    }

    @Override
    public void doOnClientConnectionError(int errorCode, String errorMessage) {
      Date now = new Date();
        System.out.println(now.toString() + " Message code: " + errorCode + ", Message: " + errorMessage);
    }
    
}
