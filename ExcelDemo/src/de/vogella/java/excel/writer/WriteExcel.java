/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.vogella.java.excel.writer;

/**
 *
 * @author Vasanti
 */


import  java.io.*;  
import  org.apache.poi.hssf.usermodel.HSSFSheet;  
import  org.apache.poi.hssf.usermodel.HSSFWorkbook; 
import  org.apache.poi.hssf.usermodel.HSSFRow;

public class WriteExcel{
    public static void main(String[]args){
try{
String filename="c:/hello.xls" ;
HSSFWorkbook hwb=new HSSFWorkbook();
HSSFSheet sheet =  hwb.createSheet("new sheet");

HSSFRow rowhead=   sheet.createRow((short)0);
rowhead.createCell((int) 0).setCellValue("SNo");
rowhead.createCell((int) 1).setCellValue("First Name");
rowhead.createCell((int) 2).setCellValue("Last Name");
rowhead.createCell((int) 3).setCellValue("Username");
rowhead.createCell((int) 4).setCellValue("E-mail");
rowhead.createCell((int) 5).setCellValue("Country");

HSSFRow row=   sheet.createRow((int)1);
row.createCell((int) 0).setCellValue("1");
row.createCell((int) 1).setCellValue("Rose");
row.createCell((int) 2).setCellValue("India");
row.createCell((int) 3).setCellValue("roseindia");
row.createCell((int) 4).setCellValue("hello@roseindia.net");
row.createCell((int) 5).setCellValue("India");

FileOutputStream fileOut =  new FileOutputStream(filename);
hwb.write(fileOut);
fileOut.close();
System.out.println("Your excel file has been generated!");

} catch ( Exception ex ) {
    System.out.println(ex);

}
    }
}
   