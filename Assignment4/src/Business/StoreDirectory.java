/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class StoreDirectory {
    private ArrayList<Stores> storeList;
    
    public StoreDirectory()
    {
    storeList=new ArrayList<Stores>();
    }
    
    public Stores addStores()
    {
        Stores store=new Stores();
        storeList.add(store);
        return store;
    }

    public ArrayList<Stores> getStoreList() {
        return storeList;
    }

    public void setStoreList(ArrayList<Stores> storeDirectory) {
        this.storeList = storeDirectory;
    }
    
    
}
