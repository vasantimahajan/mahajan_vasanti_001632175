/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author Vasanti
 */
public class Drug {
    private String name;
    private int serialNumber;
    private String expirationDate;
    private int price;
    private static int count=0;
    //private Pharmacy pharmacy;
    private int CVSprice;
    private String typeOfMedicine;
    private int idealDosage;
    private String pharmacyName;
    private int quantity;
    private int threshold;
    private int storeDrugSerialNumber;
    private String thresholdStatus;
    
    
    
   public Drug(){
       count++;
        serialNumber=count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return  name ;
    }

//    public Pharmacy getPharmacy() {
//        return pharmacy;
//    }
//
//    public void setPharmacy(Pharmacy pharmacy) {
//        this.pharmacy = pharmacy;
//    }

    public int getCVSprice() {
        return CVSprice;
    }

    public void setCVSprice(int CVSprice) {
        this.CVSprice = CVSprice;
    }

    public String getTypeOfMedicine() {
        return typeOfMedicine;
    }

    public void setTypeOfMedicine(String typeOfMedicine) {
        this.typeOfMedicine = typeOfMedicine;
    }

    public int getIdealDosage() {
        return idealDosage;
    }

    public void setIdealDosage(int idealDosage) {
        this.idealDosage = idealDosage;
    }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    } 

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public int getStoreDrugSerialNumber() {
        return storeDrugSerialNumber;
    }

    public void setStoreDrugSerialNumber(int storeDrugSerialNumber) {
        this.storeDrugSerialNumber = storeDrugSerialNumber;
    }

    public String getThresholdStatus() {
        return thresholdStatus;
    }

    public void setThresholdStatus(String thresholdStatus) {
        this.thresholdStatus = thresholdStatus;
    }
    
}
