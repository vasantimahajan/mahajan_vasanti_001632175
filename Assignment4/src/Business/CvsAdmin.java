/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class CvsAdmin {
    private ArrayList<Drug> cvsDrugCatalogue;
    private ArrayList<Pharmacy> pharmacyCatalogue;
  //  private ArrayList<Stores> storeCatalogue;
    private StoreDirectory storeDirectory;
    
    public CvsAdmin()
    {
        cvsDrugCatalogue=new ArrayList<Drug>();
        pharmacyCatalogue=new ArrayList<Pharmacy>();
     //   storeCatalogue=new ArrayList<Stores>();
        storeDirectory=new StoreDirectory();
    }
    
    public void addDrugs(Drug d)
    {
        cvsDrugCatalogue.add(d);
       
    }

    public ArrayList<Drug> getCvsDrugCatalogue() {
        return cvsDrugCatalogue;
    }

    public void setCvsDrugCatalogue(ArrayList<Drug> cvsDrugCatalogue) {
        this.cvsDrugCatalogue = cvsDrugCatalogue;
    }

    public ArrayList<Pharmacy> getPharmacyCatalogue() {
        return pharmacyCatalogue;
    }

    public void setPharmacyCatalogue(ArrayList<Pharmacy> pharmacyCatalogue) {
        this.pharmacyCatalogue = pharmacyCatalogue;
    }

//    public ArrayList<Stores> getStoreCatalogue() {
//        return storeCatalogue;
//    }
//
//    public void setStoreCatalogue(ArrayList<Stores> storeCatalogue) {
//        this.storeCatalogue = storeCatalogue;
//    }

    public StoreDirectory getStoreDirectory() {
        return storeDirectory;
    }

    public void setStoreDirectory(StoreDirectory storeDirectory) {
        this.storeDirectory = storeDirectory;
    }
    
    
}
