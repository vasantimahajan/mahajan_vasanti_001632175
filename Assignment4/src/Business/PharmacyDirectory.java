/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class PharmacyDirectory {
    private ArrayList<Pharmacy> pharmacyList;
    
    
    public PharmacyDirectory()
    {
        pharmacyList=new ArrayList<Pharmacy>();
       // cvsMasterPharmacyCatalogue=new ArrayList<Pharmacy>();
    }

    public ArrayList<Pharmacy> getPharmacyList() {
        return pharmacyList;
    }

    public void setPharmacyList(ArrayList<Pharmacy> pharmacyList) {
        this.pharmacyList = pharmacyList;
    }
    
    public Pharmacy addPharmacy(String name)
    {
       Pharmacy pharmacy=new Pharmacy();
       pharmacy.setPharmacyName(name);
       pharmacyList.add(pharmacy);  
       
       // System.err.println("pharmacy added in pharmacy dir.. "+pharmacyList);
        
        return pharmacy;
    }
    
//    public Pharmacy addPharmacyFromForm()
//    {
//        Pharmacy pharmacy=new Pharmacy(); 
//        pharmacyList.add(pharmacy);  
//        return pharmacy;
//    }
    
//     public void addToCVSPharmacyList(Pharmacy pharmacy)
//    {
//        
//        cvsMasterPharmacyCatalogue.add(pharmacy);  
//    }
//
//    public ArrayList<Pharmacy> getCvsMasterPharmacyCatalogue() {
//        return cvsMasterPharmacyCatalogue;
//    }
//
//    public void setCvsMasterPharmacyCatalogue(ArrayList<Pharmacy> cvsMasterPharmacyCatalogue) {
//        this.cvsMasterPharmacyCatalogue = cvsMasterPharmacyCatalogue;
//    }
    
}
