/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Stores {
    private String storeId;
    private String storeLocation;
    private String storeName;
    private DrugCatalogue storeDrugCatalogue;
    
    
    public Stores()
    {
        storeDrugCatalogue=new DrugCatalogue();
       
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(String storeLocation) {
        this.storeLocation = storeLocation;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    @Override
    public String toString() {
        return storeId;
    }

    public DrugCatalogue getDrugCatalogue() {
        return storeDrugCatalogue;
    }

    public void setDrugCatalogue(DrugCatalogue drugCatalogue) {
        this.storeDrugCatalogue = drugCatalogue;
    }
    
}
