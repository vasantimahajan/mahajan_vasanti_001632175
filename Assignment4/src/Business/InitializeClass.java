/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class InitializeClass {
    
    
   private PharmacyDirectory pharmacyDirectory;
    
    public PharmacyDirectory initializer(){
        
        
        pharmacyDirectory = new PharmacyDirectory();
        pharmacyDirectory.addPharmacy("Apotex");
        pharmacyDirectory.addPharmacy("Cipla");
        pharmacyDirectory.addPharmacy("Sun");
        
        Pharmacy p1=pharmacyDirectory.getPharmacyList().get(0);
        p1.getPharmacyDrugCatalogueObject().addDrug("Aspirin", "12/13/2015", 10);
        p1.getPharmacyDrugCatalogueObject().addDrug("Acetaminophen","10/31/2015",15);
        p1.getPharmacyDrugCatalogueObject().addDrug("Crocin","09/08/2016",15);
        
        Pharmacy p2=pharmacyDirectory.getPharmacyList().get(1);
        p2.getPharmacyDrugCatalogueObject().addDrug("Motrin", "12/13/2015", 25);
        p2.getPharmacyDrugCatalogueObject().addDrug("Advel","10/31/2015",10);
        p2.getPharmacyDrugCatalogueObject().addDrug("Vicks","09/08/2016",6);
        
        Pharmacy p3=pharmacyDirectory.getPharmacyList().get(2);
        p3.getPharmacyDrugCatalogueObject().addDrug("Benzonatate", "02/23/2017", 25);
        p3.getPharmacyDrugCatalogueObject().addDrug("Paracetamol","06/27/2016",10);
        p3.getPharmacyDrugCatalogueObject().addDrug("Fulyzaq","07/18/2015",6);
        
        for(Pharmacy p:pharmacyDirectory.getPharmacyList())
        {
            System.out.println("Pharmacy name:"+p.getPharmacyName());
             for(Drug d: p.getPharmacyDrugCatalogueObject().getDrugListAtPharmacy())
             {
                 System.out.println("The drugs at the pharmacy are:"+d.getName()+" "+d.getSerialNumber()+" "+d.getExpirationDate()+" "+d.getPrice());
             }
        }  
        
        for(Pharmacy p:pharmacyDirectory.getPharmacyList())
        {
            for(Drug d:p.getPharmacyDrugCatalogueObject().getDrugListAtPharmacy())
            {
                d.setPharmacyName(p.getPharmacyName());
            }
        }
        
  return  pharmacyDirectory;
}
    
    
}
