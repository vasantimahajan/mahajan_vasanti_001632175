/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author Vasanti
 */
public class Pharmacy {
    
    private String pharmacyName;
    private DrugCatalogue pharmaDrugCatalog;
    
   private PharmacyDrugCatalogue pharmacyDrugCatalogueObject;
   
   public Pharmacy()
   {
       pharmacyDrugCatalogueObject=new PharmacyDrugCatalogue();
   }

    public String getPharmacyName() {
        return pharmacyName;
    }

    public void setPharmacyName(String pharmacyName) {
        this.pharmacyName = pharmacyName;
    }

    public PharmacyDrugCatalogue getPharmacyDrugCatalogueObject() {
        return pharmacyDrugCatalogueObject;
    }

    public void setPharmacyDrugCatalogueObject(PharmacyDrugCatalogue pharmacyDrugCatalogueObject) {
        this.pharmacyDrugCatalogueObject = pharmacyDrugCatalogueObject;
    }

    @Override
    public String toString() {
        return pharmacyName;
    }
     
   
    
}
