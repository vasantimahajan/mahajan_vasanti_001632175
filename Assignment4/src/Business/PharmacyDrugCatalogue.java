/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class PharmacyDrugCatalogue {
    private ArrayList<Drug> drugListAtPharmacy;
    
   // private ArrayList<Drug> drugCatalogueAtCVS;
   
    //addDrug
    
    public PharmacyDrugCatalogue()
    {
        drugListAtPharmacy=new ArrayList<Drug>();
        //drugCatalogueAtCVS=new ArrayList<Drug>();
    }

    public ArrayList<Drug> getDrugListAtPharmacy() {
        return drugListAtPharmacy;
    }

    public void setDrugListAtPharmacy(ArrayList<Drug> drugListAtPharmacy) {
        this.drugListAtPharmacy = drugListAtPharmacy;
    }
    
    public void addDrug(String name, String date,int price)
    {
        Drug drugObject=new Drug();
        drugObject.setName(name);
        drugObject.setExpirationDate(date);
        drugObject.setPrice(price);
        drugListAtPharmacy.add(drugObject);
        
      //  System.err.println("drug list added "+drugListAtPharmacy);
    }
    public void addDrugs()
    {
        
    }
    
//    public Drug addDrugFromForm()
//    {
//        Drug drugObject=new Drug();
//        drugListAtPharmacy.add(drugObject);
//        return drugObject;
//    }
    
//     public Drug addDrugForCVSDrugList()
//    {
//        Drug drugObject=new Drug();
//       // drugCatalogueAtCVS.add(drugObject);
//        return drugObject;
//    }

//    public ArrayList<Drug> getDrugCatalogueAtCVS() {
//        return drugCatalogueAtCVS;
//    }
//
//    public void setDrugCatalogueAtCVS(ArrayList<Drug> drugCatalogueAtCVS) {
//        this.drugCatalogueAtCVS = drugCatalogueAtCVS;
//    }
//    
//    public void addDrug(Drug d)
//    {
//        drugCatalogueAtCVS.add(d);
//    }
}
