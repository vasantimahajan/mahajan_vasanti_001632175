/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class DrugCatalogue {

    private ArrayList<Drug> drugList;
    
    public DrugCatalogue()
    {
    drugList=new ArrayList<Drug>();
    }
    
    public Drug addDrugs()
    {
        Drug drug=new Drug();
        drugList.add(drug);
        return drug;
    }
    
    public void addDrugs(Drug d)
    {
         drugList.add(d);
    }

    public ArrayList<Drug> getDrugList() {
        return drugList;
    }

    public void setDrugCatalogue(ArrayList<Drug> drugCatalogue) {
        this.drugList = drugCatalogue;
    }
    
}

