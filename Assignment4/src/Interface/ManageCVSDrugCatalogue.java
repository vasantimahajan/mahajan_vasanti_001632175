/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.CvsAdmin;
import Business.PharmacyDirectory;
import java.awt.CardLayout;
import javax.swing.JPanel;

/**
 *
 * @author Vasanti
 */
public class ManageCVSDrugCatalogue extends javax.swing.JPanel {

    /**
     * Creates new form CreateCVSDrugCatalogue
     */
    private PharmacyDirectory pharmacyDirectory;
    private JPanel userProcessContainer;
    private CvsAdmin cvsAdmin;
    public ManageCVSDrugCatalogue(PharmacyDirectory pharmacyDirectory,JPanel userProcessContainer, CvsAdmin cvsAdmin) {
        initComponents();
        this.pharmacyDirectory=pharmacyDirectory;
        this.userProcessContainer=userProcessContainer;
        this.cvsAdmin=cvsAdmin;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        createCVSdrugCatalogue = new javax.swing.JButton();
        viewCVSDrugCatalogue = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        createCVSdrugCatalogue.setText("Create CVS drug catalogue");
        createCVSdrugCatalogue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createCVSdrugCatalogueActionPerformed(evt);
            }
        });

        viewCVSDrugCatalogue.setText("View CVS drug catalogue");
        viewCVSDrugCatalogue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewCVSDrugCatalogueActionPerformed(evt);
            }
        });

        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(createCVSdrugCatalogue, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(viewCVSDrugCatalogue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(147, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addComponent(createCVSdrugCatalogue)
                .addGap(40, 40, 40)
                .addComponent(viewCVSDrugCatalogue)
                .addGap(67, 67, 67)
                .addComponent(jButton1)
                .addContainerGap(52, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void createCVSdrugCatalogueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createCVSdrugCatalogueActionPerformed
        // TODO add your handling code here:
        CreateCVSDrugCatalogue create=new CreateCVSDrugCatalogue(pharmacyDirectory, userProcessContainer, cvsAdmin);
         userProcessContainer.add("CreateDrugCatalogue", create);
         CardLayout layout = (CardLayout) userProcessContainer.getLayout();
         layout.next(userProcessContainer);
    }//GEN-LAST:event_createCVSdrugCatalogueActionPerformed

    private void viewCVSDrugCatalogueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewCVSDrugCatalogueActionPerformed
        // TODO add your handling code here:
        ViewCVSDrugCatalogue view=new ViewCVSDrugCatalogue(pharmacyDirectory,userProcessContainer,cvsAdmin);
         userProcessContainer.add("ViewCVSDrugCatalogue", view);
         CardLayout layout = (CardLayout) userProcessContainer.getLayout();
         layout.next(userProcessContainer);
    }//GEN-LAST:event_viewCVSDrugCatalogueActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton createCVSdrugCatalogue;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton viewCVSDrugCatalogue;
    // End of variables declaration//GEN-END:variables
}
