/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.CvsAdmin;
import Business.Drug;
import Business.Pharmacy;
import Business.PharmacyDirectory;
import Business.PharmacyDrugCatalogue;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Vasanti
 */
public class CreateCVSDrugCatalogue extends javax.swing.JPanel {

    /**
     * Creates new form CreateCVSDrugCatalogue
     */
    private PharmacyDirectory pharmacyDirectory;
    private JPanel userProcessContainer;
    private PharmacyDrugCatalogue pharmacyDrugCatalogue;
    private CvsAdmin cvsAdmin;
  
    public CreateCVSDrugCatalogue(PharmacyDirectory pharmacyDirectory, JPanel userProcessContainer, CvsAdmin cvsAdmin) {
        initComponents();
        this.pharmacyDirectory=pharmacyDirectory;
        this.userProcessContainer=userProcessContainer;
        this.cvsAdmin=cvsAdmin;
        pharmacyDrugCatalogue=new PharmacyDrugCatalogue(); 
        populatePharmacyList();
        invisibleComponents();
   
    }
    
     private void populatePharmacyList()
    {
        choosePharmacyComboBox.removeAllItems();
        for(Pharmacy p:pharmacyDirectory.getPharmacyList())
        {
            choosePharmacyComboBox.addItem(p);
            
        }
        populateTable();
    }

    private void populateTable()
    {
        DefaultTableModel dtm=(DefaultTableModel) drugDetailsTable.getModel();
        Pharmacy pharmacy= (Pharmacy) choosePharmacyComboBox.getSelectedItem();
        dtm.setRowCount(0);
        if(pharmacy!=null)
        {
            for(Drug d: pharmacy.getPharmacyDrugCatalogueObject().getDrugListAtPharmacy())
            {
                Object row[] =new Object[3];
                row[0]=d;
                row[1]=d.getSerialNumber();
               
                row[2]=d.getExpirationDate();
                dtm.addRow(row);
                
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        header = new javax.swing.JLabel();
        choosePharmacyComboBox = new javax.swing.JComboBox();
        drugTable = new javax.swing.JScrollPane();
        drugDetailsTable = new javax.swing.JTable();
        addDrigsButton = new javax.swing.JButton();
        priceForCVSLabel = new javax.swing.JLabel();
        priceForCVSTextField = new javax.swing.JTextField();
        typeOfMedicineLabel = new javax.swing.JLabel();
        typeOfMedicineTextField = new javax.swing.JTextField();
        idealDosageLabel = new javax.swing.JLabel();
        idealDosageTextField = new javax.swing.JTextField();
        addButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        priceError = new javax.swing.JLabel();
        typeOfMedicineError = new javax.swing.JLabel();
        IdealDosageError = new javax.swing.JLabel();

        header.setText("Create CVS Mater Drug Catalogue");

        choosePharmacyComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        choosePharmacyComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                choosePharmacyComboBoxActionPerformed(evt);
            }
        });

        drugDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Drug Name", "Serial Number", "Expiration date"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Integer.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        drugTable.setViewportView(drugDetailsTable);

        addDrigsButton.setText("Add drugs");
        addDrigsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addDrigsButtonActionPerformed(evt);
            }
        });

        priceForCVSLabel.setText("Price for CVS:");

        typeOfMedicineLabel.setText("Type of medicine:");

        idealDosageLabel.setText("Ideal dosage:");

        idealDosageTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                idealDosageTextFieldActionPerformed(evt);
            }
        });

        addButton.setText("Add");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        priceError.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        priceError.setForeground(new java.awt.Color(255, 0, 0));

        typeOfMedicineError.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        typeOfMedicineError.setForeground(new java.awt.Color(255, 0, 0));

        IdealDosageError.setFont(new java.awt.Font("Tahoma", 2, 11)); // NOI18N
        IdealDosageError.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(171, 171, 171)
                        .addComponent(header))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(134, 134, 134)
                                .addComponent(addDrigsButton))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(drugTable, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(choosePharmacyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jButton1)
                                    .addGap(7, 7, 7)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(priceForCVSLabel)
                                        .addComponent(idealDosageLabel)
                                        .addComponent(typeOfMedicineLabel))
                                    .addGap(35, 35, 35)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(addButton)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(IdealDosageError, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(idealDosageTextField, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                                                .addComponent(typeOfMedicineError, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(typeOfMedicineTextField, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(priceError, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(priceForCVSTextField, javax.swing.GroupLayout.Alignment.LEADING))
                                            .addGap(0, 0, Short.MAX_VALUE))))))))
                .addContainerGap(86, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(header, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(choosePharmacyComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(drugTable, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(addDrigsButton)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(priceForCVSLabel)
                    .addComponent(priceForCVSTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(priceError, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(typeOfMedicineLabel)
                    .addComponent(typeOfMedicineTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addComponent(typeOfMedicineError, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idealDosageLabel)
                    .addComponent(idealDosageTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(IdealDosageError, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addButton)
                    .addComponent(jButton1))
                .addContainerGap(69, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void choosePharmacyComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_choosePharmacyComboBoxActionPerformed
        // TODO add your handling code here:
        populateTable();
    }//GEN-LAST:event_choosePharmacyComboBoxActionPerformed

    private void addDrigsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addDrigsButtonActionPerformed
        
        // TODO add your handling code here:
        boolean check;
        int rowSelect=drugDetailsTable.getSelectedRow();
        
        if(rowSelect>=0)
        {
            check=checkIfDrugIsAlreadyInList();
            if(check==true)
            {
                 visibleComponents();
            }
            
            else
            {
                JOptionPane.showMessageDialog(null,"The drug is alredy added to the CVS list");
            }
        }
        
        else
        {
        JOptionPane.showMessageDialog(null,"Choose a row");
        }
        
    }//GEN-LAST:event_addDrigsButtonActionPerformed

    public boolean checkIfDrugIsAlreadyInList()
    {
        boolean check=true;
        int rowSelected=drugDetailsTable.getSelectedRow();
        if(rowSelected>=0)
        {
             Drug drug= (Drug) drugDetailsTable.getValueAt(rowSelected,0);
             
             if(cvsAdmin.getCvsDrugCatalogue().isEmpty())
             {
                 check=true;
             }
             else
             {
                  for(Drug d:cvsAdmin.getCvsDrugCatalogue())
                 {
                    if(drug.getSerialNumber()==d.getSerialNumber())
                    {
                     check=false;
                    }
                 
                }
             }
            
    }
        else
        {
            JOptionPane.showMessageDialog(null,"Select a row");
        }
         return check;
    }
    
    public void  visibleComponents()
    {
        priceForCVSLabel.setVisible(true);
        priceForCVSTextField.setVisible(true);
        typeOfMedicineLabel.setVisible(true);
        typeOfMedicineTextField.setVisible(true);
        idealDosageLabel.setVisible(true);
        idealDosageTextField.setVisible(true);
        addButton.setVisible(true);
    }
    
    public void invisibleComponents()
    {
        priceForCVSLabel.setVisible(false);
        priceForCVSTextField.setVisible(false);
        typeOfMedicineLabel.setVisible(false);
        typeOfMedicineTextField.setVisible(false);
        idealDosageLabel.setVisible(false);
        idealDosageTextField.setVisible(false);
        addButton.setVisible(false);
    }
    private void idealDosageTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_idealDosageTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_idealDosageTextFieldActionPerformed

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
        // TODO add your handling code here:
       boolean result=validateFields();
        if(result==true)
        {
           
            int rowSelected=drugDetailsTable.getSelectedRow();
         if(rowSelected>=0)
            {
             Drug drug= (Drug) drugDetailsTable.getValueAt(rowSelected,0);
                    
                     drug.setCVSprice(Integer.parseInt(priceForCVSTextField.getText()));

                     drug.setTypeOfMedicine(typeOfMedicineTextField.getText());
                     
                     drug.setIdealDosage(Integer.parseInt(idealDosageTextField.getText()));
 
                     cvsAdmin.addDrugs(drug);
                     JOptionPane.showMessageDialog(null,"Drug succesfully added to the CVS catalogue");
                     invisibleComponents();
                     clearElements();
             } 
 
        else
        {
            JOptionPane.showMessageDialog(null,"Select a row");
        }
            
        }
        
       
    }//GEN-LAST:event_addButtonActionPerformed

    public boolean validateFields()
    {
      boolean errorFlag=true;
        if(priceForCVSTextField.getText().equals(""))
        {
           
            errorFlag=false;
            priceError.setText("Please enter the price for CVS");
        }
        
        else if(priceForCVSTextField.getText().matches("[0-9]+"))
        {
          
           
            priceError.setText("");
             
        }
        else
        {   errorFlag=false;
            priceError.setText("Please enter a valid price for CVS");
        }
        
        if(typeOfMedicineTextField.getText().equals(""))
        {
            errorFlag=false;
            typeOfMedicineError.setText("Please enter the type of medicine");  
        }
        else
        { 
            typeOfMedicineError.setText("");
             
        }
        
        if(idealDosageTextField.getText().equals(""))
        {
            errorFlag=false;
            IdealDosageError.setText("Please enter the ideal dosage");  
        }
        
        else if(idealDosageTextField.getText().matches("[0-9]+"))
        {
            
            IdealDosageError.setText("");
           
        }
        else
        {
            errorFlag=false;
            IdealDosageError.setText("Please enter a valid ideal dosage");  
        }
        return errorFlag;
       
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout=(CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed

    public void clearElements()
    {
        priceForCVSTextField.setText("");
        typeOfMedicineTextField.setText("");
        idealDosageTextField.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel IdealDosageError;
    private javax.swing.JButton addButton;
    private javax.swing.JButton addDrigsButton;
    private javax.swing.JComboBox choosePharmacyComboBox;
    private javax.swing.JTable drugDetailsTable;
    private javax.swing.JScrollPane drugTable;
    private javax.swing.JLabel header;
    private javax.swing.JLabel idealDosageLabel;
    private javax.swing.JTextField idealDosageTextField;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel priceError;
    private javax.swing.JLabel priceForCVSLabel;
    private javax.swing.JTextField priceForCVSTextField;
    private javax.swing.JLabel typeOfMedicineError;
    private javax.swing.JLabel typeOfMedicineLabel;
    private javax.swing.JTextField typeOfMedicineTextField;
    // End of variables declaration//GEN-END:variables
}
