/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3_part1;

import java.util.ArrayList;
import java.util.function.Supplier;

/**
 *
 * @author Vasanti
 */
public class SupplierDirectory {
    private ArrayList <Suppliers> supplierList;
    
    public SupplierDirectory()
    {
        supplierList=new ArrayList <Suppliers>();
    }

    public ArrayList <Suppliers> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(ArrayList <Suppliers> supplierList) {
        this.supplierList = supplierList;
    }
    
    public void addSuppliers(String supplierName, String supplierId)
    {
       Suppliers supplier=new Suppliers();
       supplier.setSupplierName(supplierName);
       supplier.setSupplierId(supplierId);
       supplierList.add(supplier);  
    }
    
    
}
