/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3_part1;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class ProductCatalogue {
    
     private ArrayList <Products> productList;
     
     public ProductCatalogue()
     {
         productList=new ArrayList<Products>();
     }
     
     public void addProducts(String productName,int id, int price, int availability )
     {
         Products product=new Products();
         product.setProductName(productName);
         product.setProductId(id);
         product.setProductPrice(price);
         product.setProductAvailability(availability);
         
         productList.add(product);
        
         
     }
           

    public ArrayList <Products> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList <Products> productList) {
        this.productList = productList;
    }
    
}
