/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment3_part1;

import java.util.ArrayList;

/**
 *
 * @author Vasanti
 */
public class Suppliers {
    
    private String supplierName;
    private String supplierId;
    private ProductCatalogue productCatalogue;
    
    public Suppliers()
    {
        productCatalogue=new ProductCatalogue();
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public ProductCatalogue getProductCatalogue() {
        return productCatalogue;
    }

    public void setProductCatalogue(ProductCatalogue productCatalogue) {
        this.productCatalogue = productCatalogue;
    }
   
}
