package assignment3_part1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Vasanti
 */
public class Assignment3_Part1 {

    /**
     * @param args the command line arguments
     */
  
    public static void main(String[] args) {
        // TODO code application logic here
        Assignment3_Part1 mainObject= new Assignment3_Part1();
        SupplierDirectory supplierDirectory=new SupplierDirectory();
       
        supplierDirectory.addSuppliers("Dell","Dell_Supplier");
        supplierDirectory.addSuppliers("Toshiba","Toshiba_Supplier");
        supplierDirectory.addSuppliers("HP","HP_Supplier");
        supplierDirectory.addSuppliers("Apple","Apple_Supplier");
        supplierDirectory.addSuppliers("Lenovo","Lenovo_Supplier");
      
        
        for(Suppliers s: supplierDirectory.getSupplierList()){
            
            s.getProductCatalogue().addProducts("Laptop",1000,1000,10);
            s.getProductCatalogue().addProducts("Tablet",1001,750,15);
            s.getProductCatalogue().addProducts("Charger",1002,50,25);
            s.getProductCatalogue().addProducts("Desktop",1003,800,50);
            s.getProductCatalogue().addProducts("HD TV",1004,900,20);
            s.getProductCatalogue().addProducts("Camera",1005,500,45);
            s.getProductCatalogue().addProducts("Software",1006,500,20);
            s.getProductCatalogue().addProducts("Printers",1007,100,50);
            s.getProductCatalogue().addProducts("Network switches",1008,900,20);
            s.getProductCatalogue().addProducts("Servers",1009,500,20);
        }
        
        System.out.println("The supplier directory has following suppliers:");
        System.out.println("");
        int i=0;
        for (i=0; i<5; i++)
        {
                System.out.println(i+1+" "+"Supplier Name: "+supplierDirectory.getSupplierList().get(i).getSupplierName());
                System.out.println("Supplier Id: "+supplierDirectory.getSupplierList().get(i).getSupplierId());
                System.out.println("----------------------------------------------------------------------------------------");
              
        }
        mainObject.displayProductDetails(supplierDirectory);
        
        }
    
    public void displayProductDetails(SupplierDirectory supplierDirectory)
    {
         BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         System.out.println("Enter the name of the supplier to display its product list: ");
         String supplierName = null;
         try 
         {
            supplierName = reader.readLine();
             System.out.println("The suppliers name is"+supplierName);
           
         } 
         catch (IOException e) 
         {
             System.out.println("Enter a valid supplier name");
         }
         
        for(Suppliers s : supplierDirectory.getSupplierList()){
            
             
             if(s.getSupplierName().equalsIgnoreCase(supplierName)){
                 
                 for (Products p:s.getProductCatalogue().getProductList() )
                 {
                     System.out.println("Product Name: "+p.getProductName());
                     System.out.println("Product Id: "+p.getProductId());
                     System.out.println("Product price "+p.getProductPrice());
                     System.out.println("Product availability "+p.getProductAvailability());
                     System.out.println("");
                      
                 }
             }
             
           
             
             
         }
        displayProductDetails(supplierDirectory);
    }
            
    
}
